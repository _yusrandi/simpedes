package com.userundie.simpedes.clas;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import com.userundie.simpedes.activity.SignInActivity;
import com.userundie.simpedes.aparatdesa.home.HomeActivity;
import com.userundie.simpedes.inspektorat.MainActivity;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class AlertDialogHelper {

    private SweetAlertDialog loadingDialog, successLoading, errorLoading;

    private Context mContext;

    public AlertDialogHelper(Context mContext) {
        this.mContext = mContext;
    }

    public void alertLoading(){
        loadingDialog = new SweetAlertDialog(mContext, SweetAlertDialog.PROGRESS_TYPE);
        loadingDialog.getProgressHelper().setBarColor(Color.parseColor("#226e61"));
        loadingDialog.setTitleText("Loading");
        loadingDialog.setCancelable(true);
        loadingDialog.show();
    }

    public void alertError(String message){
        errorLoading = new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE);
        errorLoading.setTitleText("Something Wrong !");
        errorLoading.setContentText(message);
        errorLoading.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {

                errorLoading.dismissWithAnimation();
            }
        });
        errorLoading.show();

    }
    public void alertSucces(String message){
        successLoading = new SweetAlertDialog(mContext, SweetAlertDialog.SUCCESS_TYPE);
        successLoading.setTitleText("Berhasil");
        successLoading.setContentText(message);

        successLoading.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                successLoading.dismissWithAnimation();

                    Intent intent = new Intent(mContext, SignInActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mContext.startActivity(intent);

//                        openDaftarDialog();

            }
        });
        successLoading.show();

    }

    public void alertSuccesCloseAlert(String message){
        successLoading = new SweetAlertDialog(mContext, SweetAlertDialog.SUCCESS_TYPE);
        successLoading.setTitleText("Berhasil");
        successLoading.setContentText(message);

        successLoading.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                successLoading.dismissWithAnimation();


//                        openDaftarDialog();

            }
        });
        successLoading.show();

    }

    public void alertSuccesCloseDialog(String message) {
        successLoading = new SweetAlertDialog(mContext, SweetAlertDialog.SUCCESS_TYPE);
        successLoading.setTitleText("Berhasil");
        successLoading.setContentText(message);

        successLoading.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                successLoading.dismissWithAnimation();

                Intent intent = new Intent(mContext, HomeActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mContext.startActivity(intent);
//                        openDaftarDialog();

            }
        });
        successLoading.show();

    }

    public void alertSuccesLogin(String message, final String rule) {
        successLoading = new SweetAlertDialog(mContext, SweetAlertDialog.SUCCESS_TYPE);
        successLoading.setTitleText("Berhasil");
        successLoading.setContentText(message);

        successLoading.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                successLoading.dismissWithAnimation();

                if(rule.equals("2")){
                    Intent intent = new Intent(mContext, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mContext.startActivity(intent);

                }else {
                    Intent intent = new Intent(mContext, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mContext.startActivity(intent);
                }

//                        openDaftarDialog();

            }
        });
        successLoading.show();

    }
    public void closeAlert(){
        loadingDialog.dismissWithAnimation();
    }

}
