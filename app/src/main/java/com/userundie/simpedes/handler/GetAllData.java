package com.userundie.simpedes.handler;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.userundie.simpedes.clas.AlertDialogHelper;
import com.userundie.simpedes.apihelper.MySingleton;
import com.userundie.simpedes.apihelper.UtilsApi;
import com.userundie.simpedes.model.AllData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetAllData {

    private String TAG = "GetAllData";
    private Context mContext;
    private String idDesa;
    private ArrayList<AllData> list = new ArrayList<>();

    private AlertDialogHelper alertDialogHelper;

    public GetAllData(Context mContext, String idDesa) {
        this.mContext = mContext;
        this.idDesa = idDesa;

        alertDialogHelper = new AlertDialogHelper(mContext);

        alertDialogHelper.alertLoading();
    }

    private void viewAllData() {
        StringRequest request = new StringRequest(Request.Method.POST, UtilsApi.URL_GET_ALL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);

                alertDialogHelper.closeAlert();

                try {
                    JSONArray jsonArray = new JSONArray(response);


                    for (int i = 0; i < jsonArray.length(); i++) {

                        final JSONObject jo = jsonArray.getJSONObject(i);

                        list.add(new AllData(
                                jo.getString("desa"),
                                jo.getString("kecamatan"),
                                jo.getString("kabupaten"),
                                jo.getString("id_user"),
                                jo.getString("nama"),
                                jo.getString("foto"),
                                jo.getString("kontak"),
                                jo.getString("jabatan"),
                                jo.getString("username"),
                                jo.getString("password"),
                                jo.getString("id_desa"),
                                jo.getString("rule"),
                                jo.getString("latitude"),
                                jo.getString("longitude"),
                                jo.getString("pekerjaan"),
                                jo.getString("gambar"),
                                jo.getString("lat_p"),
                                jo.getString("long_p"),
                                jo.getString("gambar_p"),
                                jo.getString("persen"),
                                jo.getString("detail"),
                                jo.getString("catatan"),
                                jo.getString("id_pekerjaan")
                        ));

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        MySingleton.getInstance(mContext).addToRequestQueue(request);
    }
    public ArrayList<AllData> getList() {
        return list;
    }
}
