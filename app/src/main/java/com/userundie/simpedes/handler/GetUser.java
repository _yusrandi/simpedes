package com.userundie.simpedes.handler;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.userundie.simpedes.model.DataUser;
import com.userundie.simpedes.apihelper.MySingleton;
import com.userundie.simpedes.apihelper.UtilsApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GetUser {

    private ArrayList<DataUser> list = new ArrayList<>();

    private String TAG = "GetUser";
    private String username="";
    private Context mContext;

    public GetUser(Context mContext, String username) {
        this.mContext = mContext;
        this.username = username;

        viewData(username);
    }

    private void viewData(final String username) {
        StringRequest request = new StringRequest(Request.Method.POST, UtilsApi.URL_GET_USER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);

                try {
                    JSONArray jsonArray = new JSONArray(response);


                    for (int i = 0; i < jsonArray.length(); i++) {

                        final JSONObject jo = jsonArray.getJSONObject(i);

//                        list.add(new DataUser(
//                                jo.getString("id"),
//                                jo.getString("nama"),
//                                jo.getString("foto"),
//                                jo.getString("kontak"),
//                                jo.getString("jabatan"),
//                                jo.getString("username"),
//                                jo.getString("password"),
//                                jo.getString("id_desa"),
//                                jo.getString("rule"),
//                                jo.getString("latitude"),
//                                jo.getString("longitude")
//                        ));

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();


                params.put("username", username);

                return params;

            }
        };
        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        MySingleton.getInstance(mContext).addToRequestQueue(request);
    }
    public ArrayList<DataUser> getList() {
        return list;
    }
}
