package com.userundie.simpedes.model;

public class DataUser {

    private String mKey;
    private String desa, email, gambar, jabatan, kontak, nama, password, pos, role, token;

    public DataUser() {
    }

    public DataUser(String desa, String email, String gambar, String jabatan, String kontak, String nama, String password, String pos, String role, String token) {
        this.desa = desa;
        this.email = email;
        this.gambar = gambar;
        this.jabatan = jabatan;
        this.kontak = kontak;
        this.nama = nama;
        this.password = password;
        this.pos = pos;
        this.role = role;
        this.token = token;
    }

    public String getDesa() {
        return desa;
    }

    public String getEmail() {
        return email;
    }

    public String getGambar() {
        return gambar;
    }

    public String getJabatan() {
        return jabatan;
    }

    public String getKontak() {
        return kontak;
    }

    public String getNama() {
        return nama;
    }

    public String getPassword() {
        return password;
    }

    public String getPos() {
        return pos;
    }

    public String getRole() {
        return role;
    }

    public String getToken() {
        return token;
    }

    public String getmKey() {
        return mKey;
    }

    public void setmKey(String mKey) {
        this.mKey = mKey;
    }
}
