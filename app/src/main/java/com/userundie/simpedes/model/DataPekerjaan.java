package com.userundie.simpedes.model;

public class DataPekerjaan {
    private String mKey;
    private String gambar, nama, pos, user, file;
    private String kategori;
    private int tahun;

    public DataPekerjaan() {
    }

    public DataPekerjaan(String gambar, String nama, String pos, String user, String file, String kategori, int tahun) {
        this.gambar = gambar;
        this.nama = nama;
        this.pos = pos;
        this.user = user;
        this.file = file;
        this.kategori = kategori;
        this.tahun = tahun;
    }

    public String getGambar() {
        return gambar;
    }

    public String getNama() {
        return nama;
    }

    public String getPos() {
        return pos;
    }

    public String getUser() {
        return user;
    }

    public String getFile() {
        return file;
    }

    public String getmKey() {
        return mKey;
    }

    public String getKategori() {
        return kategori;
    }

    public int getTahun() {
        return tahun;
    }

    public void setmKey(String mKey) {
        this.mKey = mKey;
    }
}
