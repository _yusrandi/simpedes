package com.userundie.simpedes.model;

public class AllData {
    private String desa, kecamatan, kabupaten;
    private String id_user, nama, foto, kontak, jabatan, username, password, id_desa, rule, latitude, longitude;
    private String pekerjaan, gambar, lat_p, long_p;
    private String gambar_p, persen, detail, catatan, id_pekerjaan;

    public AllData(String desa, String kecamatan, String kabupaten, String id_user, String nama, String foto, String kontak, String jabatan, String username, String password, String id_desa, String rule, String latitude, String longitude, String pekerjaan, String gambar, String lat_p, String long_p, String gambar_p, String persen, String detail, String catatan, String id_pekerjaan) {
        this.desa = desa;
        this.kecamatan = kecamatan;
        this.kabupaten = kabupaten;
        this.id_user = id_user;
        this.nama = nama;
        this.foto = foto;
        this.kontak = kontak;
        this.jabatan = jabatan;
        this.username = username;
        this.password = password;
        this.id_desa = id_desa;
        this.rule = rule;
        this.latitude = latitude;
        this.longitude = longitude;
        this.pekerjaan = pekerjaan;
        this.gambar = gambar;
        this.lat_p = lat_p;
        this.long_p = long_p;
        this.gambar_p = gambar_p;
        this.persen = persen;
        this.detail = detail;
        this.catatan = catatan;
        this.id_pekerjaan = id_pekerjaan;
    }

    public String getDesa() {
        return desa;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public String getId_user() {
        return id_user;
    }

    public String getNama() {
        return nama;
    }

    public String getFoto() {
        return foto;
    }

    public String getKontak() {
        return kontak;
    }

    public String getJabatan() {
        return jabatan;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getId_desa() {
        return id_desa;
    }

    public String getRule() {
        return rule;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public String getGambar() {
        return gambar;
    }

    public String getLat_p() {
        return lat_p;
    }

    public String getLong_p() {
        return long_p;
    }

    public String getGambar_p() {
        return gambar_p;
    }

    public String getPersen() {
        return persen;
    }

    public String getDetail() {
        return detail;
    }

    public String getCatatan() {
        return catatan;
    }

    public String getId_pekerjaan() {
        return id_pekerjaan;
    }
}
