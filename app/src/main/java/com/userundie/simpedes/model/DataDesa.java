package com.userundie.simpedes.model;

public class DataDesa {
    private String id;
    private String nama, kec, kab, pos;

    public DataDesa() {
    }

    public DataDesa(String nama, String kec, String kab, String pos) {
        this.nama = nama;
        this.kec = kec;
        this.kab = kab;
        this.pos = pos;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getKec() {
        return kec;
    }

    public String getKab() {
        return kab;
    }

    public String getPos() {
        return pos;
    }
}
