package com.userundie.simpedes.model;

public class DataKunjungan {
    private String mKey;
    private String desa, hari, waktu;

    public DataKunjungan() {
    }

    public DataKunjungan(String desa, String hari, String waktu) {
        this.desa = desa;
        this.hari = hari;
        this.waktu = waktu;
    }

    public void setmKey(String mKey) {
        this.mKey = mKey;
    }

    public String getmKey() {
        return mKey;
    }

    public String getDesa() {
        return desa;
    }

    public String getHari() {
        return hari;
    }

    public String getWaktu() {
        return waktu;
    }
}
