package com.userundie.simpedes.model;

public class DataProgress {

    private String mKey;
    private String catatan, detail, gambar, persen;

    public DataProgress() {
    }

    public DataProgress(String catatan, String detail, String gambar, String persen) {
        this.catatan = catatan;
        this.detail = detail;
        this.gambar = gambar;
        this.persen = persen;
    }

    public String getCatatan() {
        return catatan;
    }

    public String getDetail() {
        return detail;
    }

    public String getGambar() {
        return gambar;
    }

    public String getPersen() {
        return persen;
    }

    public String getmKey() {
        return mKey;
    }

    public void setmKey(String mKey) {
        this.mKey = mKey;
    }
}
