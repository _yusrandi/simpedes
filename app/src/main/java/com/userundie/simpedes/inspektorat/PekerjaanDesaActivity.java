package com.userundie.simpedes.inspektorat;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.userundie.simpedes.R;
import com.userundie.simpedes.aparatdesa.home.HomeActivity;
import com.userundie.simpedes.aparatdesa.home.adapter.HomeAdapter;
import com.userundie.simpedes.apihelper.MySingleton;
import com.userundie.simpedes.apihelper.UtilsApi;
import com.userundie.simpedes.clas.AlertDialogHelper;
import com.userundie.simpedes.model.DataPekerjaan;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PekerjaanDesaActivity extends AppCompatActivity {

    private String TAG = "PekerjaanDesaActivity";
    private RecyclerView mRecyclerView;
    private HomeAdapter adapter;
    private String idDesa = "";

    private ArrayList<DataPekerjaan> listAllData = new ArrayList<>();

    private AlertDialogHelper helper;

    private RelativeLayout layoutAparat, layoutPekerjaan;

    private DatabaseReference refPekerjaanDatabase;
    private RelativeLayout layoutEmpty;

    private String URL_STATISTIK = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pekerjaan_desa);

        helper = new AlertDialogHelper(PekerjaanDesaActivity.this);
        layoutAparat = findViewById(R.id.layout_aparat);
        layoutPekerjaan = findViewById(R.id.layout_pekerjaan);
        layoutEmpty = findViewById(R.id.layout_empty);

        refPekerjaanDatabase = FirebaseDatabase.getInstance().getReference().child("pekerjaan");

        idDesa = getIntent().getStringExtra("id_desa");
        Log.d(TAG, "id Desa "+idDesa);
        if(idDesa.equals("D2")){
            URL_STATISTIK = "https://drive.google.com/file/d/16crk_4LR3MNEBl6m4LAkuIjo8I-XqRWV/view?usp=sharing";
        }else if(idDesa.equals("D1")) {
            URL_STATISTIK = "https://drive.google.com/file/d/1YCwdKNCOq_Z3uCfKk_5SXzKuUeQpoOaU/view?usp=sharing";
        }else {
            Toast.makeText(PekerjaanDesaActivity.this, "Maaf Grafik Belum Tersedia", Toast.LENGTH_SHORT).show();
        }

        init();
        helper.alertLoading();

        mRecyclerView = findViewById(R.id.my_home_rv);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        layoutPekerjaan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PekerjaanDesaActivity.this, AparatLocationActivity.class);
                intent.putExtra("id_desa", idDesa);
                intent.putExtra("status", "pekerjaan");
                startActivity(intent);
            }
        });
        layoutAparat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PekerjaanDesaActivity.this, AparatLocationActivity.class);
                intent.putExtra("id_desa", idDesa);
                intent.putExtra("status", "aparat");
                startActivity(intent);
            }
        });

        findViewById(R.id.layout_search_pekerjaan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.layout_toolbar).setVisibility(View.GONE);
                findViewById(R.id.layout_search).setVisibility(View.VISIBLE);
            }
        });
        findViewById(R.id.layout_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.layout_toolbar).setVisibility(View.VISIBLE);
                findViewById(R.id.layout_search).setVisibility(View.GONE);
            }
        });

        EditText etSearch = findViewById(R.id.et_search);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    String a = s.toString().toLowerCase();
                    ArrayList<DataPekerjaan> newList = new ArrayList<>();
                    for (DataPekerjaan userInfo : listAllData) {
                        String username = userInfo.getKategori().toLowerCase();
                        if (username.contains(a)) {
                            newList.add(userInfo);
                        } else {
                            String tahun = String.valueOf(userInfo.getTahun());
                            if (tahun.contains(a)) {
                                newList.add(userInfo);
                            }
                        }
                    }
                    adapter.setFilter(newList);
                } catch (Exception e) {

                }

            }
        });

        findViewById(R.id.layout_statistik).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.addCategory(Intent.CATEGORY_BROWSABLE);
                    intent.setData(Uri.parse(URL_STATISTIK));
                    startActivity(intent);
                }catch (Exception e){

                }
            }
        });



    }

    private void init() {

        refPekerjaanDatabase.child(idDesa).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    layoutEmpty.setVisibility(View.GONE);
                    Log.d(TAG, "exists");

                    listAllData.clear();
                    helper.closeAlert();
//
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Log.d(TAG, "datasnapshot " + dataSnapshot.getKey());

                        String id = snapshot.getKey();

                        Log.d(TAG, "snapshot " + id);


                        DataPekerjaan d = snapshot.getValue(DataPekerjaan.class);

                        d.setmKey(id);

                        listAllData.add(d);

                    }
//
                    adapter = new HomeAdapter(mRecyclerView, PekerjaanDesaActivity.this, listAllData);
                    mRecyclerView.setAdapter(adapter);
//


                } else {
                    layoutEmpty.setVisibility(View.VISIBLE);
                    helper.closeAlert();
                    Log.d(TAG, "no exists");

                    listAllData.clear();
                    adapter = new HomeAdapter(mRecyclerView, PekerjaanDesaActivity.this, listAllData);
                    mRecyclerView.setAdapter(adapter);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

//
    }

    public void layoutBackClick(View view) {
        onBackPressed();
    }

}
