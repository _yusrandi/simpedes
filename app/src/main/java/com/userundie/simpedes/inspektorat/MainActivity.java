package com.userundie.simpedes.inspektorat;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.userundie.simpedes.kunjungan.KunjunganActivity;
import com.userundie.simpedes.R;
import com.userundie.simpedes.clas.AlertDialogHelper;
import com.userundie.simpedes.handler.SessionManager;
import com.userundie.simpedes.model.DataDesa;
import com.userundie.simpedes.model.DataUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback,
        com.google.android.gms.location.LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private MapView mapView;
    private GoogleMap mMap;

    private final String TAG = "MainActivity";

    private static final float DEFAULT_ZOOM = 13f;

    private ArrayList<DataDesa> listDesa = new ArrayList<>();
    private ArrayList<DataUser> listUsers = new ArrayList<>();

    private AlertDialogHelper helper;


    private LinearLayout layoutMenu;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;

    private DatabaseReference refDesaReference, refUserDatabase;

    private LinearLayout layoutLogOut;

    private SessionManager session;

    private static final long UPDATE_INTERVAL = 0;
    private static final long FASTEST_UPDATE_INTERVAL = 1000 * 120 * 1;
    private static final long MAX_WAIT_TIME = UPDATE_INTERVAL * 3;

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    private static String LAT, LNG;

    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;

    private FirebaseAuth mAuth;
    private String mCurrentUserId;

    private String inspektoratLoc, aparatLoc, inspekToken;

    private TextView tvName, tvJabatn;
    private ImageView imgUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        refDesaReference = FirebaseDatabase.getInstance().getReference().child("desa");
        refUserDatabase = FirebaseDatabase.getInstance().getReference().child("users");

        mAuth = FirebaseAuth.getInstance();
        mCurrentUserId = mAuth.getCurrentUser().getUid();

        tvName = findViewById(R.id.tv_navbar_name);
        tvJabatn = findViewById(R.id.tv_navbar_jabatan);
        imgUser = findViewById(R.id.img);

        mapView = findViewById(R.id.mapview);
        layoutMenu = findViewById(R.id.my_layout_menu);
        layoutLogOut = findViewById(R.id.layout_logout);

        drawerLayout = findViewById(R.id.drawer_layout);

        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        FirebaseApp.initializeApp(MainActivity.this);
        if (!checkPermissions()) {
            requestPermissions();
        }

        session = new SessionManager(this);
        if(session.isLoggedIn()){
            HashMap<String, String> user = session.getUserDetails();


            String role = user.get(SessionManager.RULE);
            if(role.equals("1")){
                tvJabatn.setText("Inspektorat");
            }
        }
        buildGoogleApiClient();

        layoutMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);

            }
        });

        helper = new AlertDialogHelper(MainActivity.this);
        helper.alertLoading();

        layoutLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session = new SessionManager(getApplicationContext());
                session.logoutUser();
                FirebaseAuth.getInstance().signOut();
                finishAffinity();
            }
        });

        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel =
                    new NotificationChannel("MyNotifications", "MyNotifications", NotificationManager.IMPORTANCE_DEFAULT);

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);

        }


        FirebaseMessaging.getInstance().subscribeToTopic("general")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = "Successfull";
                        if (!task.isSuccessful()) {
                            msg = "Failed";
                        }
                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
                    }
                });

        loadPositionUsers();
        initUserData();

        findViewById(R.id.layout_info).setVisibility(View.GONE);
        findViewById(R.id.layout_kunjungan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, KunjunganActivity.class));
            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Log.d(TAG, "onMapReady");

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

//                mMap.setMyLocationEnabled(true);

            }
        } else {

//            mMap.setMyLocationEnabled(true);
        }

        initData();


        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                LatLng latLng = marker.getPosition();

                for (DataDesa d : listDesa) {
                    if (d.getPos().contains(String.valueOf(latLng.latitude))) {
                        Intent intent = new Intent(MainActivity.this, PekerjaanDesaActivity.class);
                        intent.putExtra("id_desa", d.getId());
                        startActivity(intent);
                    }
                }
            }
        });

    }

    private void moveCamera(LatLng latLng, float zoom) {
        Log.d(TAG, "moveCamera  : moving camera to lat " + latLng.latitude + " and lng " + latLng.longitude);
        double latPos = latLng.latitude;
        double longPos = latLng.longitude;

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);

        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));


    }

    private void addMarkers() {

        String[] latlng = listDesa.get(0).getPos().split(",");


//        moveCamera(new LatLng(Double.valueOf(latlng[0]),Double.valueOf(latlng[1])), DEFAULT_ZOOM);

        String[] mLatlng;
        for (DataDesa d : listDesa) {
            mLatlng = d.getPos().split(",");

            Log.d(TAG, "Latitude " + mLatlng[0]);
            mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(mLatlng[0]), Double.valueOf(mLatlng[1]))).title(d.getNama()).snippet(d.getKec()).icon(BitmapDescriptorFactory.fromResource(R.mipmap.house)));
        }
//        mMap.addMarker(new MarkerOptions().position(new LatLng(-5.0788004, 119.5292097)).title("hahahhaa").snippet("lalalalla").icon(BitmapDescriptorFactory.fromResource(R.mipmap.locationpin)));

    }

    private void initData() {

        refDesaReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                helper.closeAlert();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    DataDesa d = snapshot.getValue(DataDesa.class);
                    d.setId(snapshot.getKey());

                    listDesa.add(d);
                }

                addMarkers();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                helper.closeAlert();
            }
        });

    }
    void initUserData(){
        refUserDatabase.child(mCurrentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){
                    Log.d(TAG, "User "+dataSnapshot.child("nama").getValue());
                    tvName.setText(""+dataSnapshot.child("nama").getValue());

                    try {
                        Glide.with(MainActivity.this)
                                .load(dataSnapshot.child("gambar").getValue())
                                .apply(new RequestOptions()
                                        .placeholder(R.mipmap.kamera)
                                        .error(R.mipmap.kamera))
                                .into(imgUser);
                    }catch (Exception e){

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        Log.d(TAG, "Location update started ..............: ");
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            Snackbar.make(
                    findViewById(R.id.TambahPekerjaanActivity),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    })
                    .show();
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        mLocationRequest.setInterval(UPDATE_INTERVAL);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Sets the maximum time when batched location updates are delivered. Updates may be
        // delivered sooner than this interval.
        mLocationRequest.setMaxWaitTime(MAX_WAIT_TIME);
    }

    private void buildGoogleApiClient() {
        if (mGoogleApiClient != null) {
            return;
        }
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        Log.i(TAG, "GoogleApiClient connected");
        startLocationUpdates();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

        LAT = String.valueOf(location.getLatitude());
        LNG = String.valueOf(location.getLongitude());

        Log.d(TAG, "onLocationChanged at Lat " + LAT);
        moveCamera(new LatLng(location.getLatitude(), location.getLongitude()), DEFAULT_ZOOM);

        try {
            refUserDatabase.child(mCurrentUserId).child("pos").setValue(LAT + "," + LNG).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Log.d(TAG, "onLocationChanged : Success update location");


                    mMap.clear();

                    mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(LAT), Double.valueOf(LNG))).title("My Posotion").snippet("Inspektorat").icon(BitmapDescriptorFactory.fromResource(R.mipmap.location)));


                    addMarkers();

                }
            });
        } catch (Exception e) {

        }
    }

    private void loadPositionUsers() {
        refUserDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d(TAG, "onDataChange");

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    String nama = snapshot.child("nama").getValue().toString();
                    String role = snapshot.child("role").getValue().toString();
                    String pos = snapshot.child("pos").getValue().toString();
                    String token = snapshot.child("token").getValue().toString();
                    Log.d(TAG, "snapshot : " + nama + " Role " + role + " Position " + pos);


                    DataUser user = snapshot.getValue(DataUser.class);
                    listUsers.add(user);

                    if (role.equals("1")) {
                        inspektoratLoc = pos;
                        inspekToken = token;
                        Log.d(TAG, "snapshot inspektorat : " + nama + " Role " + role + " Position " + pos);
                    } else {

                    }


                }

                calculatiLocUsers();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void calculatiLocUsers() {
        for (DataUser d : listUsers) {

            Log.d(TAG, " calculatiLocUsers : " + d.getPos());
            if (d.getPos().equals(inspektoratLoc)) {

            } else {

                try {
                    String[] insSplit = inspektoratLoc.split(",");
                    String[] apaSplit = d.getPos().split(",");

                    LatLng start = new LatLng(Double.valueOf(insSplit[0]), Double.valueOf(insSplit[1]));
                    LatLng end = new LatLng(Double.valueOf(apaSplit[0]), Double.valueOf(apaSplit[1]));

                    double distance = CalculationByDistance(start, end);
                    Log.d(TAG, "calculatiLocUsers : Distance " + distance);

                    if (distance < 1) {

                        sendFCMPush(inspekToken, "Woee ada aparat yang datang", "Warning !");
                    }

                } catch (Exception e) {

                }
            }
        }
    }

    public double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;//radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        double kmInDec = Double.valueOf(newFormat.format(km));
        double meter = kmInDec / 1000;
        double meterInDec = Double.valueOf(newFormat.format(meter));
        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec + " Meter   " + String.format("%.3f", meter));

        return kmInDec;
    }

    private void sendFCMPush(String tokenUser, String msg, String title) {

        final String SERVER_KEY = "AAAA22RXmtM:APA91bFfF8YrTU-aQ3rv5lKZrLl7m1m5StXQukfhS2k1vO7S6lBlcP6HllUYiV4mep2e_9jvp9OLQEIMLDHfZbfnobXHW98UYTyqKCQdDnxTHNh3NjuoPicFDi76dZnzMhLoWPYNMwPQ";
        String token = tokenUser;
        String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";

        JSONObject obj = null;
        JSONObject objData = null;
        JSONObject dataobjData = null;

        try {
            obj = new JSONObject();
            objData = new JSONObject();

            objData.put("body", msg);
            objData.put("title", title);
            objData.put("sound", "default");
            objData.put("icon", "icon_name"); //   icon_name
            objData.put("tag", token);
            objData.put("priority", "high");

            dataobjData = new JSONObject();
            dataobjData.put("text", msg);
            dataobjData.put("title", title);

            obj.put("to", token);
            //obj.put("priority", "high");

            obj.put("notification", objData);
            obj.put("data", dataobjData);
            Log.d(TAG, obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, API_URL_FCM, obj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response + "");

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error + "");
                        NetworkResponse response = error.networkResponse;
                        if (error instanceof ServerError && response != null) {
                            try {
                                String res = new String(response.data,
                                        HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                // Now you can use any deserializer to make sense of data
                                JSONObject obj = new JSONObject(res);
                            } catch (UnsupportedEncodingException e1) {
                                // Couldn't properly decode data to string
                                e1.printStackTrace();
                            } catch (JSONException e2) {
                                // returned data is not JSONObject?
                                e2.printStackTrace();
                            }
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "key=" + SERVER_KEY);
//                params.put("Content-Type", "application/json");
                params.put("Content-Type", "application/json; charset=utf-8");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 1000 * 60;// 60 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsObjRequest.setRetryPolicy(policy);
        requestQueue.add(jsObjRequest);
    }

    public void myLocationClick(View view) {
        moveCamera(new LatLng(Double.valueOf(LAT), Double.valueOf(LNG)), DEFAULT_ZOOM);
    }
}
