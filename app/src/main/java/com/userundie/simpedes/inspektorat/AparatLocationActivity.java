package com.userundie.simpedes.inspektorat;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.userundie.simpedes.R;
import com.userundie.simpedes.apihelper.MySingleton;
import com.userundie.simpedes.apihelper.UtilsApi;
import com.userundie.simpedes.clas.AlertDialogHelper;
import com.userundie.simpedes.model.DataDesa;
import com.userundie.simpedes.model.DataPekerjaan;
import com.userundie.simpedes.model.DataUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AparatLocationActivity extends AppCompatActivity implements OnMapReadyCallback, com.google.android.gms.location.LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private MapView mapView;
    private GoogleMap mMap;

    private final String TAG = "AparatLocationActivity";

    private static final float DEFAULT_ZOOM = 16f;

    private ArrayList<DataUser> listUser = new ArrayList<>();
    private ArrayList<DataPekerjaan> listAparat = new ArrayList<>();

    private AlertDialogHelper helper;

    private String idDesa = "", status = "";

    private DatabaseReference refUsersDatabase, refPekerjaanDatabase, refDesaDatabase, refProgressDatabase;
    private FirebaseAuth mAuth;
    private String mCurrentUserId;

    private String namaDesa, detailPekerjaan, catatanInspektorat, progress;

    private static final long UPDATE_INTERVAL = 0;
    private static final long FASTEST_UPDATE_INTERVAL = 1000 * 120 * 1;
    private static final long MAX_WAIT_TIME = UPDATE_INTERVAL * 3;

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    private static String LAT, LNG;

    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aparat_location);

        mAuth = FirebaseAuth.getInstance();
        mCurrentUserId = mAuth.getCurrentUser().getUid();

        mapView = findViewById(R.id.mapview);
        helper = new AlertDialogHelper(AparatLocationActivity.this);
        helper.alertLoading();

        refUsersDatabase = FirebaseDatabase.getInstance().getReference().child("users");
        refPekerjaanDatabase = FirebaseDatabase.getInstance().getReference().child("pekerjaan");
        refDesaDatabase = FirebaseDatabase.getInstance().getReference().child("desa");
        refProgressDatabase = FirebaseDatabase.getInstance().getReference().child("progress");

        idDesa = getIntent().getStringExtra("id_desa");
        status = getIntent().getStringExtra("status");
        Log.d(TAG, "id Desa " + idDesa);

        initDesa(idDesa);

        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }

        buildGoogleApiClient();
    }

    private void initDesa(String id) {

        refDesaDatabase.child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {
                    String nama = dataSnapshot.child("nama").getValue().toString();
                    namaDesa = nama;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        Log.d(TAG, "onMapReady");

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

//                mMap.setMyLocationEnabled(true);

            }
        } else {

//            mMap.setMyLocationEnabled(true);
        }

        if (status.equals("aparat")) {
            initDataUser();

        } else {
            initDataAparat();
        }


        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                LatLng latLng = marker.getPosition();

                if (status.equals("aparat")) {
                    for (DataUser d : listUser) {
                        if (d.getPos().contains(String.valueOf(latLng.latitude))) {

                            dialogAparat(d);
                        }
                    }
                } else {
                    for (DataPekerjaan d : listAparat) {
                        if (d.getPos().contains(String.valueOf(latLng.latitude))) {

                            dialogPekerjaan(d.getmKey(), d.getNama());
                        }
                    }
                }

            }
        });


    }


    private void moveCamera(LatLng latLng, float zoom) {
        Log.d(TAG, "moveCamera  : moving camera to lat " + latLng.latitude + " and lng " + latLng.longitude);
        double latPos = latLng.latitude;
        double longPos = latLng.longitude;

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);

        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));


    }

    private void addMarkers() {

        try {
            if (status.equals("aparat")) {
                String[] latlang = listUser.get(0).getPos().split(",");
                moveCamera(new LatLng(Double.valueOf(latlang[0]), Double.valueOf(latlang[1])), DEFAULT_ZOOM);
                String[] latlangG;
                for (DataUser d : listUser) {
                    latlangG = d.getPos().split(",");

                    mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(latlangG[0]), Double.valueOf(latlangG[1]))).title(d.getNama()).snippet(d.getJabatan()).icon(BitmapDescriptorFactory.fromResource(R.mipmap.position)));
                }
            } else {
                String[] latlang = listAparat.get(0).getPos().split(",");
                moveCamera(new LatLng(Double.valueOf(latlang[0]), Double.valueOf(latlang[1])), DEFAULT_ZOOM);
                String[] latlangG;
                for (DataPekerjaan d : listAparat) {
                    latlangG = d.getPos().split(",");

                    mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(latlangG[0]), Double.valueOf(latlangG[1]))).title(d.getNama()).snippet(d.getUser()).icon(BitmapDescriptorFactory.fromResource(R.mipmap.house)));
                }
            }
        } catch (Exception e) {

        }

    }

    private void initDataUser() {
        refUsersDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                helper.closeAlert();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    DataUser user = snapshot.getValue(DataUser.class);
                    user.setmKey(snapshot.getKey());

                    if (user.getRole().equals("2")) {
                        if (user.getDesa().equals(idDesa)) {
                            Log.d(TAG, "termasuk");

                            listUser.add(user);
                        }
                    }
                }

                addMarkers();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                helper.closeAlert();
            }
        });

    }

    private void initDataAparat() {
        refPekerjaanDatabase.child(idDesa).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                helper.closeAlert();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    DataPekerjaan user = snapshot.getValue(DataPekerjaan.class);
                    user.setmKey(snapshot.getKey());

                    listAparat.add(user);

                }

                addMarkers();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                helper.closeAlert();
            }
        });

    }

    private void dialogAparat(DataUser d) {
        Dialog dialogDetail = new Dialog(this);

        dialogDetail.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogDetail.setContentView(R.layout.dialog_user);

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        Window window = dialogDetail.getWindow();
        layoutParams.copyFrom(window.getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialogDetail.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setAttributes(layoutParams);
        dialogDetail.setCancelable(true);

        TextView tvNama = dialogDetail.findViewById(R.id.nama);
        TextView tvEmail = dialogDetail.findViewById(R.id.email);
        TextView tvKontak = dialogDetail.findViewById(R.id.kontak);
        TextView tvJabatan = dialogDetail.findViewById(R.id.jabatan);

        ImageView img = dialogDetail.findViewById(R.id.img);

        tvNama.setText(d.getNama());
        tvEmail.setText(d.getEmail());
        tvKontak.setText(d.getKontak());
        tvJabatan.setText(d.getJabatan());

        Glide.with(this)
                .load(d.getGambar())
                .apply(new RequestOptions()
                        .placeholder(R.mipmap.kamera)
                        .error(R.mipmap.kamera))
                .into(img);

        dialogDetail.show();
    }

    private void dialogPekerjaan(String id, String nama) {
        Dialog dialogDetail = new Dialog(this);

        dialogDetail.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogDetail.setContentView(R.layout.dialog_pekerjaan);

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        Window window = dialogDetail.getWindow();
        layoutParams.copyFrom(window.getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialogDetail.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setAttributes(layoutParams);
        dialogDetail.setCancelable(true);

        TextView namaPekerjaan = dialogDetail.findViewById(R.id.nama_pekerjaan);
        TextView namaDesa = dialogDetail.findViewById(R.id.nama_desa);
        final TextView tvDetail = dialogDetail.findViewById(R.id.detail);
        final TextView tvCatatan = dialogDetail.findViewById(R.id.catatan);
        final TextView progress = dialogDetail.findViewById(R.id.progress);

        initProgress(id);

        namaDesa.setText(this.namaDesa);
        namaPekerjaan.setText("desa " + nama);
//        detail.setText(detailPekerjaan);
//        catatan.setText(catatanInspektorat);
        progress.setText(this.progress + " %");

        refProgressDatabase.child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    String persen = snapshot.child("persen").getValue().toString();
                    String detail = snapshot.child("detail").getValue().toString();
                    String catatan = snapshot.child("catatan").getValue().toString();

                    progress.setText(persen);
                    tvCatatan.setText(catatan);
                    tvDetail.setText(detail);


                    Log.d(TAG, "persen " + persen);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        dialogDetail.show();
    }

    private void initProgress(String id) {


    }

    private void buildGoogleApiClient() {
        if (mGoogleApiClient != null) {
            return;
        }
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        mLocationRequest.setInterval(UPDATE_INTERVAL);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Sets the maximum time when batched location updates are delivered. Updates may be
        // delivered sooner than this interval.
        mLocationRequest.setMaxWaitTime(MAX_WAIT_TIME);
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        Log.d(TAG, "Location update started ..............: ");
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "GoogleApiClient connected");
        startLocationUpdates();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

        LAT = String.valueOf(location.getLatitude());
        LNG = String.valueOf(location.getLongitude());

        Log.d(TAG, "onLocationChanged at Lat " + LAT);

        try {
            refUsersDatabase.child(mCurrentUserId).child("pos").setValue(LAT + "," + LNG).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Log.d(TAG, "onLocationChanged : Success update location");


                    mMap.clear();

                    mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(LAT), Double.valueOf(LNG))).title("My Posotion").snippet("Inspektorat").icon(BitmapDescriptorFactory.fromResource(R.mipmap.location)));

                    moveCamera(new LatLng(Double.valueOf(LAT), Double.valueOf(LNG)), DEFAULT_ZOOM);
                    addMarkers();

                }
            });
        } catch (Exception e) {

        }
    }

    public void myLocationClick(View view) {
        moveCamera(new LatLng(Double.valueOf(LAT), Double.valueOf(LNG)), DEFAULT_ZOOM);
    }

    public void layoutBackClick(View view) {
        onBackPressed();
    }
}
