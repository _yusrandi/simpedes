package com.userundie.simpedes.apihelper;

public class UtilsApi {
    public static final String ip = "http://192.168.2.1/simpedes/";
    public static final String URL_VIEW_DESA = ip+"desa.php";
    public static final String URL_REGISTRASI = ip+"registrasi.php";
    public static final String URL_TAMBAH_PEKERJAAN = ip+"tambah_pekerjaan.php";
    public static final String URL_TAMBAH_PROGRESS = ip+"tambah_progress.php";
    public static final String URL_PROGRESS_UPDATE = ip+"progress_update.php";
    public static final String URL_LOGIN = ip+"login.php";
    public static final String URL_UPDATE_LOCATION = ip+"update_location.php";
    public static final String URL_GET_USER = ip+"getuser.php";
    public static final String URL_GET_USER_DESA = ip+"getuserdesa.php";
    public static final String URL_GET_PEKERJAAN = ip+"getpekerjaan.php";
    public static final String URL_GET_PROGRESS = ip+"getprogress.php";
    public static final String URL_GET_ALL = ip+"view_all.php";
    public static final String URL_GET_IMAGES = ip+"images/";

}
