package com.userundie.simpedes.kunjungan;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.userundie.simpedes.R;
import com.userundie.simpedes.model.DataDesa;
import com.userundie.simpedes.model.DataKunjungan;

import java.util.ArrayList;
import java.util.List;

public class KunjunganAdapter extends RecyclerView.Adapter<KunjunganAdapter.KunjunganViewHolder> {

    private static final String TAG = "KunjunganAdapter";

    private Context context;
    private ArrayList<String> list;
    private ArrayList<DataKunjungan> listKunjungan;
    private ArrayList<DataKunjungan> listDetailKunjungan;
    private OnItemClickListener mListener;
    private DatabaseReference refDesaReference, refUserDatabase, refKunjunganDatabase;

    private RecyclerView mRecyclerView;

    public KunjunganAdapter(Context context, ArrayList<String> list, ArrayList<DataKunjungan> listKunjungan) {
        this.context = context;
        this.list = list;
        this.listKunjungan = listKunjungan;
    }

    @NonNull
    @Override
    public KunjunganViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_kunjungan, viewGroup, false);
        refDesaReference = FirebaseDatabase.getInstance().getReference().child("desa");
        refKunjunganDatabase = FirebaseDatabase.getInstance().getReference().child("kunjungan");

        mRecyclerView = v.findViewById(R.id.item_kunj_rv);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
        RecyclerView.LayoutManager manager = new GridLayoutManager(context, 1);
        mRecyclerView.setLayoutManager(manager);

        listDetailKunjungan = new ArrayList<>();

        return new KunjunganViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull final KunjunganViewHolder holder, final int i) {



        final String id = list.get(i);
        Log.d(TAG, "Id "+id);

        for(DataKunjungan d : listKunjungan){

            if (id.equals(d.getDesa())){
                Log.d(TAG, "desa for "+d.getDesa());

                listDetailKunjungan.add(d);

            }


        }
        DetailKunjunganAdapter detailKunjunganAdapter = new DetailKunjunganAdapter(context, listDetailKunjungan);
        mRecyclerView.setAdapter(detailKunjunganAdapter);
        detailKunjunganAdapter.notifyDataSetChanged();


        refDesaReference.child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String nama = dataSnapshot.child("nama").getValue().toString();
                holder.tvName.setText(nama);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "list size "+list.size());
        Log.d(TAG, "listKunjungan size "+listKunjungan.size());
        return list.size();
    }

    public class KunjunganViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public TextView tvName;
        public RecyclerView mRecyclerView;

        public KunjunganViewHolder(@NonNull View view) {
            super(view);

            tvName = view.findViewById(R.id.item_kunj_nama);
            mRecyclerView = view.findViewById(R.id.item_kunj_rv);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    mListener.onItemClick(position);
                }
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);

    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public void setFilter(List<String> newList){
        list = new ArrayList<>();
        list.addAll(newList);
        notifyDataSetChanged();
    }

}
