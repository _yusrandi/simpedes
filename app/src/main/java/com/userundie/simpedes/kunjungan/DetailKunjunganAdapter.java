package com.userundie.simpedes.kunjungan;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.userundie.simpedes.R;
import com.userundie.simpedes.model.DataKunjungan;

import java.util.ArrayList;
import java.util.List;

public class DetailKunjunganAdapter extends RecyclerView.Adapter<DetailKunjunganAdapter.KunjunganViewHolder> {

    private static final String TAG = "DetailKunjunganAdapter";

    private Context context;
    private ArrayList<DataKunjungan> list;
    private OnItemClickListener mListener;
    private DatabaseReference refDesaReference, refUserDatabase, refKunjunganDatabase;


    public DetailKunjunganAdapter(Context context, ArrayList<DataKunjungan> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public KunjunganViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_detail_kunjungan, viewGroup, false);
        refDesaReference = FirebaseDatabase.getInstance().getReference().child("desa");

        return new KunjunganViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull final KunjunganViewHolder holder, final int i) {


        DataKunjungan d = list.get(i);
        holder.tvHari.setText(d.getHari());
        holder.tvWaktu.setText(d.getWaktu());




    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "list size "+list.size());
        return list.size();
    }

    public class KunjunganViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public TextView tvHari, tvWaktu;

        public KunjunganViewHolder(@NonNull View view) {
            super(view);

            tvHari = view.findViewById(R.id.detail_kunj_hari);
            tvWaktu = view.findViewById(R.id.detail_kunj_waktu);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    mListener.onItemClick(position);
                }
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);

    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public void setFilter(List<DataKunjungan> newList){
        list = new ArrayList<>();
        list.addAll(newList);
        notifyDataSetChanged();
    }

}
