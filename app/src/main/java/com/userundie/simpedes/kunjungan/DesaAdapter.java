package com.userundie.simpedes.kunjungan;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.userundie.simpedes.R;
import com.userundie.simpedes.model.DataDesa;

import java.util.ArrayList;
import java.util.List;

public class DesaAdapter extends RecyclerView.Adapter<DesaAdapter.DesaViewHolder> {

    private static final String TAG = "DesaAdapter";

    private Context context;
    private ArrayList<DataDesa> list;
    private OnItemClickListener mListener;

    public DesaAdapter(Context context, ArrayList<DataDesa> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public DesaViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_desa, viewGroup, false);

        return new DesaViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull DesaViewHolder holder, final int i) {

        DataDesa d  = list.get(i);
        holder.tvName.setText(d.getNama());
        holder.tvKec.setText(d.getKec());


    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "list size "+list.size());
        return list.size();
    }

    public class DesaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public TextView tvName, tvKec;

        public DesaViewHolder(@NonNull View view) {
            super(view);

            tvName = view.findViewById(R.id.desa_nama);
            tvKec = view.findViewById(R.id.desa_kec);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    mListener.onItemClick(position);
                }
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);

    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public void setFilter(List<DataDesa> newList){
        list = new ArrayList<>();
        list.addAll(newList);
        notifyDataSetChanged();
    }

}
