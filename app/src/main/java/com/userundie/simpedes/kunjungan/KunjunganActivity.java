package com.userundie.simpedes.kunjungan;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.userundie.simpedes.R;
import com.userundie.simpedes.activity.SignInActivity;
import com.userundie.simpedes.clas.AlertDialogHelper;
import com.userundie.simpedes.handler.SessionManager;
import com.userundie.simpedes.inspektorat.MainActivity;
import com.userundie.simpedes.model.DataDesa;
import com.userundie.simpedes.model.DataKunjungan;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class KunjunganActivity extends AppCompatActivity {

    private final static String TAG = "KunjunganActivity";

    private Dialog dialog, dialogDesa;
    private TextView tvHari, tvWaktu, tvDesa;
    private DesaAdapter desaAdapter;
    private ArrayList<DataDesa> listDesa;

    private DatabaseReference refDesaReference, refUserDatabase, refKunjunganDatabase;
    private FirebaseAuth mAuth;
    private String mCurrentUserId;
    private AlertDialogHelper helper;

    private SweetAlertDialog successLoading;

    private String resDesa, resHari, resWaktu;
    private RecyclerView mRecyclerView;
    private KunjunganAdapter kunjunganAdapter;
    private ArrayList<String> listKunjungan;
    private ArrayList<DataKunjungan> listDetailKunjungan;

    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kunjungan);
        listDesa = new ArrayList<>();
        listKunjungan = new ArrayList<>();
        listDetailKunjungan = new ArrayList<>();
        refDesaReference = FirebaseDatabase.getInstance().getReference().child("desa");
        refUserDatabase = FirebaseDatabase.getInstance().getReference().child("users");
        refKunjunganDatabase = FirebaseDatabase.getInstance().getReference().child("kunjungan");

        mAuth = FirebaseAuth.getInstance();
        mCurrentUserId = mAuth.getCurrentUser().getUid();

        helper = new AlertDialogHelper(KunjunganActivity.this);
        mRecyclerView = findViewById(R.id.kunjungan_rv);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
        RecyclerView.LayoutManager manager = new GridLayoutManager(this, 1);
        mRecyclerView.setLayoutManager(manager);


        findViewById(R.id.kunjungan_fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDialogKunjungan();
            }
        });

        session = new SessionManager(getApplicationContext());
        if (session.isLoggedIn()) {


            HashMap<String, String> user = session.getUserDetails();


            String role = user.get(SessionManager.RULE);
            String desa = user.get(SessionManager.DESA);

            Log.d(TAG, "Role "+role+" desa "+desa);

            if (role.equals("1")){
                initKunjungan();
            }else {
                initKunjungan(refKunjunganDatabase.child(desa));
                findViewById(R.id.kunjungan_fab).setVisibility(View.GONE);
            }
        }

    }

    private void initKunjungan(DatabaseReference child) {
        child.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot snapshot1 : dataSnapshot.getChildren()){
                    String hari = snapshot1.child("hari").getValue().toString();

                    Log.d(TAG, "snapshot1 "+snapshot1.getKey()+" hari "+hari);
                    DataKunjungan d = snapshot1.getValue(DataKunjungan.class);
                    d.setmKey(dataSnapshot.getKey());

                    listDetailKunjungan.add(d);

                }

                DetailKunjunganAdapter detailKunjunganAdapter = new DetailKunjunganAdapter(KunjunganActivity.this, listDetailKunjungan);
                mRecyclerView.setAdapter(detailKunjunganAdapter);
                detailKunjunganAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    private void initKunjungan() {

        refKunjunganDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                listKunjungan.clear();
                listDetailKunjungan.clear();

                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Log.d(TAG, "snapshot "+snapshot.getKey());

                    listKunjungan.add(snapshot.getKey());

                    for(DataSnapshot snapshot1 : snapshot.getChildren()){
                        String hari = snapshot1.child("hari").getValue().toString();

                        Log.d(TAG, "snapshot1 "+snapshot1.getKey());
                        DataKunjungan d = snapshot1.getValue(DataKunjungan.class);
                        d.setmKey(snapshot.getKey());

                        listDetailKunjungan.add(d);

                    }

                }

                kunjunganAdapter = new KunjunganAdapter(KunjunganActivity.this, listKunjungan, listDetailKunjungan);
                mRecyclerView.setAdapter(kunjunganAdapter);
                kunjunganAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    void addDialogKunjungan() {
        dialog = new Dialog(this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_kunjungan);

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        layoutParams.copyFrom(window.getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setAttributes(layoutParams);
        dialog.setCancelable(false);

        tvHari = dialog.findViewById(R.id.kunjungan_tv_hari);
        tvWaktu = dialog.findViewById(R.id.kunjungan_tv_waktu);
        tvDesa = dialog.findViewById(R.id.kunjungan_tv_desa);

        dialog.findViewById(R.id.kunjungan_btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });
        dialog.findViewById(R.id.kunjungan_btn_create).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                resHari = tvHari.getText().toString();
                resWaktu = tvWaktu.getText().toString();

                String mKey = resHari+resWaktu;

                HashMap<String, String> map = new HashMap<>();
                map.put("desa", resDesa);
                map.put("hari", resHari);
                map.put("waktu", resWaktu);

                refKunjunganDatabase.child(resDesa).child(mKey).setValue(map).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        alertSucces("Berhasil Menambahkan Kunjungan");
                    }
                });
            }
        });
        dialog.findViewById(R.id.kunjungan_tv_hari).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogDatePicker();
            }
        });
        dialog.findViewById(R.id.kunjungan_tv_waktu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTimePicker();
            }
        });
        dialog.findViewById(R.id.kunjungan_tv_desa).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                helper.alertLoading();
                dialogDesa();
            }
        });
        dialog.show();
    }

    void dialogDesa() {
        dialogDesa = new Dialog(this);

        dialogDesa.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogDesa.setContentView(R.layout.dialog_desa);

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        Window window = dialogDesa.getWindow();
        layoutParams.copyFrom(window.getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialogDesa.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setAttributes(layoutParams);
        dialogDesa.setCancelable(false);

        final RecyclerView rv = dialogDesa.findViewById(R.id.desa_rv);
        rv.setHasFixedSize(true);
        rv.setNestedScrollingEnabled(false);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
        RecyclerView.LayoutManager manager = new GridLayoutManager(this, 1);
        rv.setLayoutManager(manager);


        refDesaReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                helper.closeAlert();
                listDesa.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    DataDesa d = snapshot.getValue(DataDesa.class);
                    d.setId(snapshot.getKey());

                    listDesa.add(d);
                }

                desaAdapter = new DesaAdapter(KunjunganActivity.this, listDesa);
                rv.setAdapter(desaAdapter);
                desaAdapter.notifyDataSetChanged();
                desaAdapter.setOnItemClickListener(new DesaAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int position) {

                        DataDesa d = listDesa.get(position);
                        tvDesa.setText(d.getNama());
                        resDesa = d.getId();
                        dialogDesa.dismiss();
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        dialogDesa.show();
    }

    void dialogDatePicker() {

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePickerDialog;

        mDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker date, int year, int month, int day) {
                Toast.makeText(KunjunganActivity.this, "Tanggal di pilih " + date.getYear() +
                        " / " + (date.getMonth() + 1) +
                        " / " + date.getDayOfMonth(), Toast.LENGTH_SHORT).show();

                tvHari.setText(date.getDayOfMonth() + "-" + (date.getMonth() + 1) + "-" + date.getYear());

            }
        }, year, month, day);
        mDatePickerDialog.setTitle("Pilih Hari");
        mDatePickerDialog.show();

    }

    void dialogTimePicker() {

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                Toast.makeText(KunjunganActivity.this, selectedHour + ":" + selectedMinute, Toast.LENGTH_SHORT).show();
                tvWaktu.setText(selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Pilih Waktu");
        mTimePicker.show();

    }

    public void alertSucces(String message){
        successLoading = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
        successLoading.setTitleText("Berhasil");
        successLoading.setContentText(message);

        successLoading.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                successLoading.dismissWithAnimation();

                dialog.dismiss();

            }
        });
        successLoading.show();

    }
}
