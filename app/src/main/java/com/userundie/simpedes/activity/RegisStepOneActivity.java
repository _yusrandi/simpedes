package com.userundie.simpedes.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.userundie.simpedes.R;
import com.userundie.simpedes.clas.AlertDialogHelper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class RegisStepOneActivity extends AppCompatActivity {

    private String TAG = "RegisStepOneActivity";
    private ImageView img;

    private static int PICK_IMAGE_REQUEST = 1;
    private static int CAMERA_PIC_REQUEST = 2;

    private Uri filePath;
    private Bitmap bitmap = null;

    public static String GAMBAR = "";
    public static Uri GAMBAR_URI;
    private AlertDialogHelper helper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regis_step_one);

        FirebaseApp.initializeApp(this);

        img = findViewById(R.id.img);
        helper = new AlertDialogHelper(RegisStepOneActivity.this);



        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission();
                CropImage.activity(filePath)
                        .setAspectRatio(1, 1)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(RegisStepOneActivity.this);

            }
        });

    }

    void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED &&
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED &&
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:" + getPackageName()));
            startActivity(intent);
            return;
        }
    }

    public void gotoStepTwoClick(View view) {


        if (!GAMBAR.equals("")) {
            startActivity(new Intent(this, RegisStepTwoActivity.class));
        } else {
            helper.alertError("Silahkan pilih foto terlebih dahulu");
//            Toast.makeText(RegisStepOneActivity.this, "Silahkan pilih foto terlebih dahulu ", Toast.LENGTH_LONG).show();
        }


    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        try {
            filePath = data.getData();


            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
                filePath = data.getData();


                CropImage.activity(filePath)
                        .setAspectRatio(1, 1)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);

            }
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);

                if (resultCode == RESULT_OK) {

                    Uri resultUri = result.getUri();
                    GAMBAR_URI = resultUri;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(RegisStepOneActivity.this.getContentResolver(), resultUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    File f = new File(resultUri.getPath());
                    long size = f.length() / 1024;


                    Log.d(TAG, " path " + resultUri.getPath() + " Size " + f.length() / 1024);

                    if (size < 500) {

                        img.setImageBitmap(bitmap);
                        GAMBAR = getStringImage(bitmap);
                        Log.d(TAG, "result gambar "+GAMBAR);

                    } else
                        Toast.makeText(RegisStepOneActivity.this, "Maaf size maximum 500 Kb", Toast.LENGTH_SHORT).show();

                }
            }

            if (requestCode == CAMERA_PIC_REQUEST && resultCode == RegisStepOneActivity.this.RESULT_OK) {
//                bitmap = (Bitmap) data.getExtras().get("data");
//                filePath = getImageUri(RegisStepOneActivity.this, bitmap);
//                Log.d(TAG, bitmap.toString() + ", Uri " + filePath);
//                img.setImageBitmap(bitmap);



//                CropImage.activity(filePath)
//                        .setAspectRatio(1, 1)
//                        .setGuidelines(CropImageView.Guidelines.ON)
//                        .start(this);

            }
        } catch (Exception e) {
            Toast.makeText(this, "Silahkan pilih foto", Toast.LENGTH_SHORT).show();
        }

    }

    private void showPictureDialog(){

        checkPermission();

        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select picture from gallery",
                "Take picture from camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                chooseVideoFromGallary();
                                break;
                            case 1:
                                takeVideoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void chooseVideoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT,
                android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }

    private void takeVideoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_PIC_REQUEST);
    }

    private Uri getImageUri(Context context, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

}
