package com.userundie.simpedes.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.userundie.simpedes.R;
import com.userundie.simpedes.aparatdesa.home.HomeActivity;
import com.userundie.simpedes.clas.AlertDialogHelper;
import com.userundie.simpedes.apihelper.MySingleton;
import com.userundie.simpedes.apihelper.UtilsApi;
import com.userundie.simpedes.handler.SessionManager;
import com.userundie.simpedes.inspektorat.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class SignInActivity extends AppCompatActivity {

    private String TAG = "SignInActivity";
    private EditText etUser, etPass;

    private String USERNAME="", PASSWORD="";

    private SessionManager session;
    private SharedPreferences sharedPreferences;

    public static String ID_DESA="";

    private FirebaseAuth mAuth;

    private DatabaseReference mUserDatabase;
    private DatabaseReference mRootRef;

    private SweetAlertDialog loadingDialog, successLoading, errorLoading;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        etPass = findViewById(R.id.etSignInPassword);
        etUser = findViewById(R.id.etSignInUsername);

        mAuth = FirebaseAuth.getInstance();
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("users");



        sharedPreferences = getSharedPreferences("SIMPEDES", MODE_PRIVATE);

//        session.checkLogin();
        session = new SessionManager(getApplicationContext());
    }

    public void gotoSignUp(View view) {
        startActivity(new Intent(this, RegisStepOneActivity.class));
    }

    public void signInClick(View view) {

        USERNAME = etUser.getText().toString();
        PASSWORD = etPass.getText().toString();

        if(!USERNAME.equals("") && !PASSWORD.equals("")){
            signIn();

        }else alertError("Harap Mengisi semua kolom");
    }

    private void signIn(){
//
        alertLoading();
        mAuth.signInWithEmailAndPassword(USERNAME, PASSWORD).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {

                    loadingDialog.dismissWithAnimation();

                    final String current_user_id = mAuth.getCurrentUser().getUid();

                    Log.d(TAG, "User ID " + current_user_id);


                    String deviceToken = FirebaseInstanceId.getInstance().getToken();

                    mUserDatabase.child(current_user_id).child("token").setValue(deviceToken);
                    mUserDatabase.child(current_user_id).addValueEventListener(new ValueEventListener() {
                       @Override
                       public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                           String name = dataSnapshot.child("nama").getValue().toString();
                           String rule = dataSnapshot.child("role").getValue().toString();
                           String desa = dataSnapshot.child("desa").getValue().toString();

                           session.createLoginSession(rule, desa);

                           Log.d(TAG, "user name "+name);

                           alertSuccesLogin("Selamat datang ", rule);

                       }

                       @Override
                       public void onCancelled(@NonNull DatabaseError databaseError) {

                       }
                   });
//                    helper.alertSucces("Selamat, anda berhasil masuk");

                } else {
                    loadingDialog.dismissWithAnimation();
                    alertError("Maaf anda belum terdaftar");
                }
            }

        });

    }

    public void alertLoading(){
        loadingDialog = new SweetAlertDialog(SignInActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loadingDialog.getProgressHelper().setBarColor(Color.parseColor("#226e61"));
        loadingDialog.setTitleText("Loading");
        loadingDialog.setCancelable(true);
        loadingDialog.show();
    }
    public void alertError(String message){
        errorLoading = new SweetAlertDialog(SignInActivity.this, SweetAlertDialog.ERROR_TYPE);
        errorLoading.setTitleText("Something Wrong !");
        errorLoading.setContentText(message);
        errorLoading.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {

                errorLoading.dismissWithAnimation();
            }
        });
        errorLoading.show();

    }

    public void alertSuccesLogin(String message, final String rule) {
        successLoading = new SweetAlertDialog(SignInActivity.this, SweetAlertDialog.SUCCESS_TYPE);
        successLoading.setTitleText("Berhasil");
        successLoading.setContentText(message);

        successLoading.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                successLoading.dismissWithAnimation();

                if(rule.equals("2")){
                    Intent intent = new Intent(SignInActivity.this, HomeActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();

                }else {
                    Intent intent = new Intent(SignInActivity.this, MainActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }

//                        openDaftarDialog();

            }
        });
        try {
            successLoading.show();
        }catch (Exception e){

        }

    }
}
