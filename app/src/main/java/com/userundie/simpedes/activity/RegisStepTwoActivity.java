package com.userundie.simpedes.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.userundie.simpedes.R;
import com.userundie.simpedes.clas.AlertDialogHelper;
import com.userundie.simpedes.model.DataDesa;
import com.userundie.simpedes.apihelper.MySingleton;
import com.userundie.simpedes.apihelper.UtilsApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import fr.ganfra.materialspinner.MaterialSpinner;

public class RegisStepTwoActivity extends AppCompatActivity {

    private Button mButton;
    private MaterialSpinner mSpinner;

    private ArrayList<String> listDesa;
    private ArrayList<DataDesa> listResultDesa;
    private String TAG = "RegisStepTwoActivity";

    private EditText etNama, etKontak, etJabatan;

    public static String NAMA="", KONTAK="", JABATAN="", DESA="";

    private AlertDialogHelper helper;

    private DatabaseReference refDesa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regis_step_two);

        FirebaseApp.initializeApp(this);
        refDesa = FirebaseDatabase.getInstance().getReference().child("desa");

        mButton = findViewById(R.id.my_btn_step_two);
        etJabatan = findViewById(R.id.etJabatan);
        etKontak = findViewById(R.id.etKontak);
        etNama = findViewById(R.id.etNama);




        helper = new AlertDialogHelper(RegisStepTwoActivity.this);

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NAMA = etNama.getText().toString();
                KONTAK = etKontak.getText().toString();
                JABATAN = etJabatan.getText().toString();

                if(!NAMA.equals("") && !KONTAK.equals("") && !JABATAN.equals("") && !DESA.equals("")){
                    startActivity(new Intent(RegisStepTwoActivity.this, RegisStepThreeActivity.class));
                }else {
                    helper.alertError("Silahkan Mengisi semua kolom");
                }
            }
        });

        listDesa = new ArrayList<>();
        listResultDesa = new ArrayList<>();

        loadData();

        mSpinner = findViewById(R.id.spinner);

        ArrayAdapter<String> adapterDesa = new ArrayAdapter<String>(RegisStepTwoActivity.this, R.layout.dropdown_hint_item, listDesa);
        adapterDesa.setDropDownViewResource(R.layout.dropdown_hint_item);
        mSpinner.setAdapter(adapterDesa);

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String result = parent.getItemAtPosition(position).toString();

                for(DataDesa d : listResultDesa){
                    if(result.equals(d.getNama())){
                        Log.d(TAG, "Result Spinner "+d.getId());
                        DESA = d.getId();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void loadData() {


        refDesa.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    DataDesa desa = snapshot.getValue(DataDesa.class);

                    desa.setId(snapshot.getKey());
                    listResultDesa.add(desa);

                    listDesa.add(desa.getNama());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
