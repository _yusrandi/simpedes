package com.userundie.simpedes.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.userundie.simpedes.R;
import com.userundie.simpedes.clas.AlertDialogHelper;
import com.userundie.simpedes.apihelper.MySingleton;
import com.userundie.simpedes.apihelper.UtilsApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisStepThreeActivity extends AppCompatActivity implements com.google.android.gms.location.LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private String TAG = "RegisStepThreeActivity";
    private Button mButton;
    private EditText etUsername, etPassword, etRePassword;

    private String USERNAME = "", PASSWORD = "", LATITUDE = "", LONGITUDE = "";
    private AlertDialogHelper helper;

    private LocationManager locationManager;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 10 meters

    // The minimum time between updates in milliseconds
//    private static final long MIN_TIME_BW_UPDATES = 0; // 1 minute
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    private static final long UPDATE_INTERVAL = 0;
    private static final long FASTEST_UPDATE_INTERVAL = 1000 * 120 * 1;
    private static final long MAX_WAIT_TIME = UPDATE_INTERVAL * 3;


    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;

    private Uri resultUri;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase, usersDatabase, desaDatabase;
    private StorageReference mImageStorage;
    private StorageTask mUploadTask;

    private static final String UPLOAD_PATH_STORAGE = "profile_users";

    private String uid = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regis_step_three);

        mButton = findViewById(R.id.my_btn_regist);

        helper = new AlertDialogHelper(RegisStepThreeActivity.this);

        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        etRePassword = findViewById(R.id.etReTypePassword);

        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance();
        mImageStorage = FirebaseStorage.getInstance().getReference().child(UPLOAD_PATH_STORAGE);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        resultUri = RegisStepOneActivity.GAMBAR_URI;
        Log.d(TAG, "result uri "+resultUri);


        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                USERNAME = etUsername.getText().toString();
                PASSWORD = etPassword.getText().toString();

                if(resultUri != null){
                    if (!USERNAME.equals("") && !PASSWORD.equals("")) {

                        if (!LATITUDE.equals("")) {
                            insertData();


                        }else {
                            helper.alertError("Harap Menunggu, Masih mencari lokasi anda");
                        }

                    } else {
                        helper.alertError("Harap Mengisi semua kolom");
                    }
                }else {

                }
            }
        });

//        getMyLocation();

        checkPermission();
        buildGoogleApiClient();

    }

    void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED ) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:" + getPackageName()));
            startActivity(intent);
            return;
        }
    }
    private void buildGoogleApiClient() {
        if (mGoogleApiClient != null) {
            return;
        }
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        mLocationRequest.setInterval(UPDATE_INTERVAL);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Sets the maximum time when batched location updates are delivered. Updates may be
        // delivered sooner than this interval.
        mLocationRequest.setMaxWaitTime(MAX_WAIT_TIME);
    }

    private void insertData() {

        helper.alertLoading();

        mAuth.createUserWithEmailAndPassword(USERNAME, PASSWORD).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){


                    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                    uid = currentUser.getUid();

                    final StorageReference mStorageReference = mImageStorage.child(uid+".jpg");

                    mUploadTask = mStorageReference.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                            if(task.isSuccessful()){
                                final Task<Uri> urlTask = mUploadTask.continueWithTask(new Continuation() {
                                    @Override
                                    public Object then(@NonNull Task task) throws Exception {
                                        if (!task.isSuccessful()) {
                                            throw task.getException();
                                        }

                                        // Continue with the task to get the download URL
                                        return mStorageReference.getDownloadUrl();
                                    }
                                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Uri> task) {

                                        if(task.isSuccessful()){
                                            Uri uri = task.getResult();

                                            String uploaded_image_url = uri.toString();

                                            HashMap<String, String> usMap = new HashMap<>();

                                                usersDatabase = mDatabase.child("users").child(uid);

                                                usMap.put("nama", RegisStepTwoActivity.NAMA);
                                                usMap.put("kontak", RegisStepTwoActivity.KONTAK);
                                                usMap.put("jabatan", RegisStepTwoActivity.JABATAN);
                                                usMap.put("email", USERNAME);
                                                usMap.put("gambar", uploaded_image_url);
                                                usMap.put("password", PASSWORD);
                                                usMap.put("role", "2");
                                                usMap.put("desa", RegisStepTwoActivity.DESA);
                                                usMap.put("pos", LATITUDE+","+LONGITUDE);
                                                usMap.put("token", "");
                                            usersDatabase.setValue(usMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {

                                                    helper.closeAlert();
                                                    helper.alertSucces("Thank you for join with us");
                                                }
                                            });
                                        }

                                    }
                                });

                            }else {

                                helper.closeAlert();
                                Toast.makeText(RegisStepThreeActivity.this, "Terjadi kesalahan, silahkan coba lagi", Toast.LENGTH_LONG).show();

                            }
                        }
                    });
                }else {
                    helper.closeAlert();
                    helper.alertError("Terjadi kesalahan, silahkan coba lagi");
                }
            }
        });

    }

    void startApp() {
        Intent intent = new Intent(RegisStepThreeActivity.this, SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public void getMyLocation() {
        Log.d(TAG, "GetMyLocation ");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
//                    mMap.clear();
                    double lat = location.getLatitude();
                    double lng = location.getLongitude();


                    Log.d(TAG, "LATLANG Network" + lat);


                }


                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {
                    Log.d(TAG, "onStatusChanged Network");

                }

                @Override
                public void onProviderEnabled(String s) {
                    Log.d(TAG, "onProviderEnabled Network");

                }

                @Override
                public void onProviderDisabled(String s) {
                    Log.d(TAG, "onProviderDisabled Network");


                }
            });


        } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
//                    mMap.clear();
                    double lat = location.getLatitude();
                    double lng = location.getLongitude();

                    Log.d(TAG, "LATLANG Gps " + lat);

                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            });
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "GoogleApiClient connected");
        startLocationUpdates();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i(TAG, "onLocationChanged");

        LATITUDE = String.valueOf(location.getLatitude());
        LONGITUDE = String.valueOf(location.getLongitude());


    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        Log.d(TAG, "Location update started ..............: ");
    }
}
