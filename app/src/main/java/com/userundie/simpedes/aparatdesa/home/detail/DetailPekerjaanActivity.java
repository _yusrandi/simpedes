package com.userundie.simpedes.aparatdesa.home.detail;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.userundie.simpedes.R;
import com.userundie.simpedes.aparatdesa.home.TambahPekerjaanActivity;
import com.userundie.simpedes.aparatdesa.home.adapter.DetailAdapter;
import com.userundie.simpedes.clas.AlertDialogHelper;
import com.userundie.simpedes.apihelper.MySingleton;
import com.userundie.simpedes.apihelper.UtilsApi;
import com.userundie.simpedes.handler.SessionManager;
import com.userundie.simpedes.model.DataPekerjaan;
import com.userundie.simpedes.model.DataProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DetailPekerjaanActivity extends AppCompatActivity implements DetailAdapter.OnClickListener {

    private String TAG = "DetailPekerjaanActivity";
    private RecyclerView mRecyclerView;
    private DetailAdapter adapter;

    private ArrayList<DataProgress> list = new ArrayList<>();
    private String ID_PEKERJAAN = "";

    private AlertDialogHelper helper;

    private ImageView img;

    private static int PICK_IMAGE_REQUEST = 1;
    private Uri filePath, resultUri;
    private Bitmap bitmap = null;

    private String resultGambar = "", resultPersen = "", resultIdP = "", resultDetail = "", resultId = "", resultImageName = "", ID_DESA, ROLE;

    private DatabaseReference refProgressdatabase, refPekerjaanDatabase;
    private StorageReference mImageStorage;
    private StorageTask mUploadTask;

    private static final String UPLOAD_PATH_STORAGE = "pekerjaan";
    private Dialog dialogDetail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pekerjaan);

        helper = new AlertDialogHelper(DetailPekerjaanActivity.this);
        helper.alertLoading();
        ID_PEKERJAAN = getIntent().getStringExtra("id_pekerjaan");

        refProgressdatabase = FirebaseDatabase.getInstance().getReference().child("progress");
        refPekerjaanDatabase = FirebaseDatabase.getInstance().getReference().child("pekerjaan");
        mImageStorage = FirebaseStorage.getInstance().getReference().child(UPLOAD_PATH_STORAGE);


        mRecyclerView = findViewById(R.id.my_detail_rv);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        init();

        SessionManager session = new SessionManager(getApplicationContext());
        if (session.isLoggedIn()) {


            HashMap<String, String> user = session.getUserDetails();


            ID_DESA = user.get("desa");
            ROLE = user.get("rule");

            Log.i(TAG, "Desa " + ID_DESA + " Role " + ROLE);


        }


    }

    private void init() {

        refProgressdatabase.child(ID_PEKERJAAN).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                list.clear();
                helper.closeAlert();
//
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Log.d(TAG, "datasnapshot " + dataSnapshot.getKey());

                    String id = snapshot.getKey();

                    Log.d(TAG, "snapshot " + id);


                    DataProgress d = snapshot.getValue(DataProgress.class);

                    d.setmKey(id);

                    list.add(d);

                }

                adapter = new DetailAdapter(mRecyclerView, DetailPekerjaanActivity.this, list);
                adapter.setOnClickListener(DetailPekerjaanActivity.this);
                mRecyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void viewUbahDetail(final DataProgress progress, final String status, final int pos) {

        dialogDetail = new Dialog(this);

        dialogDetail.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogDetail.setContentView(R.layout.dialog_ubah_detail_pekerjaan);

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        Window window = dialogDetail.getWindow();
        layoutParams.copyFrom(window.getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialogDetail.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setAttributes(layoutParams);
        dialogDetail.setCancelable(true);

//        final TextView tvJum, name, ttl, email, jab, telp, telpAlt, alamt;
        Button btn;
        final EditText etDetail;


        btn = dialogDetail.findViewById(R.id.btn_tambah);
        img = dialogDetail.findViewById(R.id.img_add);
        etDetail = dialogDetail.findViewById(R.id.et_detail);

        RelativeLayout layoutBrowse = dialogDetail.findViewById(R.id.layout_browse);


//        ttl = dialog.findViewById(R.id.my_tv_dialog_ttl);

//        ttl.setText(user.getTtl());

        if (status.equals("belum")) {
            btn.setText("Tambah");

        } else {
            btn.setText("Ubah");

            Glide.with(this)
                    .load(progress.getGambar())
                    .apply(new RequestOptions()
                            .placeholder(R.mipmap.jembatan)
                            .error(R.mipmap.jembatan))
                    .into(img);

            if (ROLE.equals("1")) {
                etDetail.setHint("Catatan Progress");
                etDetail.setText(progress.getCatatan());

            } else {
                etDetail.setText(progress.getDetail());

            }


        }

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(ROLE.equals("2")){
                    if(status.equals("belum")){
                        resultDetail = etDetail.getText().toString();
                        resultPersen = String.valueOf(50*pos);
                        resultIdP = ID_PEKERJAAN;

                        if(!resultDetail.equals("")){
                            progressInsertData();

                            helper.alertLoading();

                        }else helper.alertError("Silahkan mengisi semua field");
                    }else {
                        resultId = progress.getmKey();
                        resultDetail = etDetail.getText().toString();
                        resultIdP = ID_PEKERJAAN;
                        resultPersen = progress.getPersen();

                        if(resultGambar.equals("")){
                            resultImageName = progress.getGambar();
                        }
                        helper.alertLoading();
                        progressUpdateData();


                    }

                }else {
                    String catatan = etDetail.getText().toString();
                    resultPersen = progress.getPersen();

                    if(resultPersen.equals("50")){
                        resultPersen = "050";
                    }
                    Log.d(TAG, "Result Persen "+resultPersen);
                    helper.alertLoading();
                    refProgressdatabase.child(ID_PEKERJAAN).child(ID_PEKERJAAN + resultPersen).child("catatan").setValue(catatan).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            helper.closeAlert();
                            helper.alertSuccesCloseAlert("Data Berhasil ditambahkan");
                            dialogDetail.dismiss();
                        }
                    });
                }
            }
        });


        layoutBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

        dialogDetail.show();
    }

    private void showFileChooser() {
        CropImage.activity(filePath)
                .setAspectRatio(200, 100)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);

    }

    private String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        try {
            filePath = data.getData();


            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
                filePath = data.getData();


//            try {
//                bitmap = MediaStore.Images.Media.getBitmap(RegisStepOneActivity.this.getContentResolver(), filePath);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }

                CropImage.activity(filePath)
                        .setAspectRatio(200, 100)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);

            }
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);

                if (resultCode == RESULT_OK) {

                    Uri resultUri = result.getUri();
                    this.resultUri = resultUri;

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

//                mProgressBar.setVisibility(View.VISIBLE);
//                uploadFile(result, bitmap);


                    File f = new File(resultUri.getPath());
                    long size = f.length() / 1024;


                    Log.d(TAG, " path " + resultUri.getPath() + " Size " + f.length() / 1024);

                    if (size < 1024) {

                        img.setImageBitmap(bitmap);

                        File ff = new File(String.valueOf(resultUri));
                        resultGambar = getStringImage(bitmap);
                        resultImageName = ff.getName();

                    } else
                        Toast.makeText(DetailPekerjaanActivity.this, "Maaf size maximum 1024 Kb", Toast.LENGTH_SHORT).show();


//                Log.d(TAG, "gambar "+GAMBAR);

                }
            }
        } catch (Exception e) {

        }

    }


    private void viewDetail(DataProgress progress) {

        final Dialog dialog = new Dialog(this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_detail_pekerjaan);

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        layoutParams.copyFrom(window.getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setAttributes(layoutParams);
        dialog.setCancelable(true);

        final TextView tvDetail, tvCatatan;
        ImageView img;


        tvDetail = dialog.findViewById(R.id.tv_detail);
        tvCatatan = dialog.findViewById(R.id.tv_catatan);
        img = dialog.findViewById(R.id.img_detail);

        tvDetail.setText(progress.getDetail());
        tvCatatan.setText(progress.getCatatan());

        Glide.with(this)
                .load(progress.getGambar())
                .apply(new RequestOptions()
                        .placeholder(R.mipmap.jembatan)
                        .error(R.mipmap.jembatan))
                .into(img);


        dialog.show();
    }


    @Override
    public void onPilihClick(int position, String status) {
        DataProgress progress = null;
        try {

            progress = list.get(position);
            viewUbahDetail(progress, status, position);

        } catch (Exception e) {

            viewUbahDetail(progress, status, position);
        }
    }

    @Override
    public void onLihatClick(int position) {
        DataProgress progress = list.get(position);
        viewDetail(progress);
//        Toast.makeText(DetailPekerjaanActivity.this, "ini lihat klik "+position, Toast.LENGTH_SHORT).show();

    }

    private void progressInsertData() {


        final StorageReference mStorageReference = mImageStorage.child(resultImageName + ".jpg");
        mUploadTask = mStorageReference.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                if (task.isSuccessful()) {
                    final Task<Uri> urlTask = mUploadTask.continueWithTask(new Continuation() {
                        @Override
                        public Object then(@NonNull Task task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }

                            // Continue with the task to get the download URL
                            return mStorageReference.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {

                            if (task.isSuccessful()) {
                                Uri uri = task.getResult();

                                final String uploaded_image_url = uri.toString();

                                final HashMap<String, String> usMap = new HashMap<>();


                                HashMap<String, String> proMap = new HashMap<>();

                                proMap.put("persen", resultPersen);
                                proMap.put("detail", resultDetail);
                                proMap.put("catatan", "");
                                proMap.put("gambar", uploaded_image_url);

                                if(resultPersen.equals("50")){
                                    resultPersen = "0"+resultPersen;
                                }

                                refProgressdatabase.child(ID_PEKERJAAN).child(ID_PEKERJAAN + resultPersen).setValue(proMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {


                                        refPekerjaanDatabase.child(ID_DESA).child(ID_PEKERJAAN).child("gambar").setValue(uploaded_image_url).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                helper.alertSuccesCloseAlert("Data Berhasil ditambahkan");
                                                dialogDetail.dismiss();
                                            }
                                        });

                                    }
                                });


                            }

                        }
                    });

                } else {

                    helper.closeAlert();
                    Toast.makeText(DetailPekerjaanActivity.this, "Terjadi kesalahan, silahkan coba lagi", Toast.LENGTH_LONG).show();

                }

            }
        });

    }

    private void progressUpdateData() {

        if (resultUri == null) {
            Log.d(TAG, "Result Uri null");
            HashMap<String, Object> proMap = new HashMap<>();

            proMap.put("persen", resultPersen);
            proMap.put("detail", resultDetail);

            if(resultPersen.equals("50")){
                resultPersen = "0"+resultPersen;
            }

            refProgressdatabase.child(ID_PEKERJAAN).child(ID_PEKERJAAN + resultPersen).updateChildren(proMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    helper.alertSuccesCloseAlert("Data Berhasil ditambahkan");
                    helper.closeAlert();
                    dialogDetail.dismiss();

                }
            });

        } else {
            final StorageReference mStorageReference = mImageStorage.child(resultImageName + ".jpg");
            mUploadTask = mStorageReference.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                    if (task.isSuccessful()) {
                        final Task<Uri> urlTask = mUploadTask.continueWithTask(new Continuation() {
                            @Override
                            public Object then(@NonNull Task task) throws Exception {
                                if (!task.isSuccessful()) {
                                    throw task.getException();
                                }

                                // Continue with the task to get the download URL
                                return mStorageReference.getDownloadUrl();
                            }
                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {

                                if (task.isSuccessful()) {
                                    Uri uri = task.getResult();

                                    final String uploaded_image_url = uri.toString();

                                    final HashMap<String, String> usMap = new HashMap<>();


                                    HashMap<String, Object> proMap = new HashMap<>();

                                    proMap.put("persen", resultPersen);
                                    proMap.put("detail", resultDetail);
                                    proMap.put("catatan", "");
                                    proMap.put("gambar", uploaded_image_url);

                                    if(resultPersen.equals("50")){
                                        resultPersen = "0"+resultPersen;
                                    }

                                    refProgressdatabase.child(ID_PEKERJAAN).child(ID_PEKERJAAN + resultPersen).updateChildren(proMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            refPekerjaanDatabase.child(ID_DESA).child(ID_PEKERJAAN).child("gambar").setValue(uploaded_image_url).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    helper.alertSuccesCloseAlert("Data Berhasil ditambahkan");
                                                    dialogDetail.dismiss();
                                                }
                                            });

                                        }
                                    });


                                }

                            }
                        });

                    } else {

                        helper.closeAlert();
                        Toast.makeText(DetailPekerjaanActivity.this, "Terjadi kesalahan, silahkan coba lagi", Toast.LENGTH_LONG).show();

                    }

                }
            });
        }

    }

    public void layoutBackClick(View view) {
        onBackPressed();
    }


}
