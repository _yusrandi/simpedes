package com.userundie.simpedes.aparatdesa.home.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.userundie.simpedes.R;
import com.userundie.simpedes.aparatdesa.home.detail.DetailPekerjaanActivity;
import com.userundie.simpedes.apihelper.UtilsApi;
import com.userundie.simpedes.handler.SessionManager;
import com.userundie.simpedes.inspektorat.MainActivity;
import com.userundie.simpedes.model.AllData;
import com.userundie.simpedes.model.DataPekerjaan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {

    private static final String TAG = "HomeAdapter";

    private RecyclerView mRecyclerView;
    private Context mContext;
    private ArrayList<DataPekerjaan> list;

    private DatabaseReference refUserDatabase;
    private FirebaseAuth mAuth;
    private String mCurrentUserId;

    private static String latPos, lngPos;
    private SessionManager sessionManager;

    private String ROLE;

    public HomeAdapter(RecyclerView mRecyclerView, Context mContext, ArrayList<DataPekerjaan> list) {
        this.mRecyclerView = mRecyclerView;
        this.mContext = mContext;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pekerjaan, viewGroup, false);

        mAuth = FirebaseAuth.getInstance();
        mCurrentUserId = mAuth.getCurrentUser().getUid();
        refUserDatabase = FirebaseDatabase.getInstance().getReference().child("users");

        sessionManager = new SessionManager(mContext);
        if(sessionManager.isLoggedIn()){
            HashMap<String, String> user = sessionManager.getUserDetails();


            ROLE = user.get(SessionManager.RULE);
        }
        initUserData();

        MyViewHolder ViewHolder = new MyViewHolder(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = mRecyclerView.getChildLayoutPosition(view);
//
//                dataService data = list.get(position);
//                getDetailDialog(data);

//                viewDetail(data);

                DataPekerjaan pekerjaan = list.get(position);
                Intent intent = new Intent(mContext, DetailPekerjaanActivity.class);
                intent.putExtra("id_pekerjaan", pekerjaan.getmKey());
                mContext.startActivity(intent);


            }
        });

        return new HomeAdapter.MyViewHolder(itemView);
    }

    void initUserData(){
        refUserDatabase.child(mCurrentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){
                    Log.d(TAG, "User "+dataSnapshot.child("nama").getValue());

                    String pos = dataSnapshot.child("pos").getValue().toString();

                    String split[] = pos.split(",");

                    latPos = split[0];
                    lngPos = split[1];
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {

        final DataPekerjaan d = list.get(i);
        myViewHolder.nama.setText(list.get(i).getNama());
        myViewHolder.kategori.setText(list.get(i).getKategori()+" | "+list.get(i).getTahun());

        if(ROLE.equals("2")){
            myViewHolder.layoutGo.setVisibility(View.GONE);

        }
        Glide.with(mContext)
                .load(list.get(i).getGambar())
                .apply(new RequestOptions()
                        .placeholder(R.mipmap.jembatan)
                        .error(R.mipmap.jembatan))
                .into(myViewHolder.img);

        myViewHolder.layoutPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "This is layout view pdf  "+list.get(i).getFile(), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse(d.getFile()), "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Intent newIntent = Intent.createChooser(intent, "Open File");
                mContext.startActivity(newIntent);
            }
        });

        myViewHolder.layoutGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "This is layout go on "+list.get(i).getPos(),Toast.LENGTH_SHORT).show();

                String split[] = d.getPos().split(",");

                LatLng latLngPos = new LatLng(Double.valueOf(latPos),Double.valueOf(lngPos));
                LatLng latLngDir = new LatLng(Double.valueOf(split[0]),Double.valueOf(split[1]));

                showNavigation(latLngPos, latLngDir);

            }
        });

    }

    public void showNavigation(LatLng latPos, LatLng latDir) {

        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://ditu.google.cn/maps?f=d&source=s_d" +
                        "&saddr=" + latPos.latitude + "," + latPos.longitude +
                        "&daddr=" + latDir.latitude + "," + latDir.longitude +
                        "&hl=zh&t=m&dirflg=d"
                ));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK & Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        mContext.startActivity(intent);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView nama, kategori;
        public ImageView img;

        public  LinearLayout layoutPdf, layoutGo;


        public MyViewHolder(View view) {
            super(view);

            nama = (TextView) view.findViewById(R.id.item_pekerjaan_nama);
            kategori = (TextView) view.findViewById(R.id.item_pekerjaan_kategori);
            img = view.findViewById(R.id.images);

            layoutGo = view.findViewById(R.id.layout_go);
            layoutPdf = view.findViewById(R.id.layout_pdf);


        }


    }

    public void setFilter(List<DataPekerjaan> newList){
        list = new ArrayList<>();
        list.addAll(newList);
        notifyDataSetChanged();
    }
}
