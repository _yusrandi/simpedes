package com.userundie.simpedes.aparatdesa.home;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.userundie.simpedes.R;
import com.userundie.simpedes.aparatdesa.home.adapter.HomeAdapter;
import com.userundie.simpedes.clas.AlertDialogHelper;
import com.userundie.simpedes.apihelper.MySingleton;
import com.userundie.simpedes.apihelper.UtilsApi;
import com.userundie.simpedes.handler.SessionManager;
import com.userundie.simpedes.inspektorat.MainActivity;
import com.userundie.simpedes.kunjungan.KunjunganActivity;
import com.userundie.simpedes.model.DataPekerjaan;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HomeActivity extends AppCompatActivity implements com.google.android.gms.location.LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private String TAG = "HomeActivity";
    private LinearLayout layoutMenu;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;

    private RecyclerView mRecyclerView;
    private HomeAdapter adapter;

    private TextView tvName, tvJabatan;
    private ImageView imgUser;

    private LinearLayout layoutLogOut, layoutTambah;

    private SessionManager session;

    private String idDesa = "";

    private AlertDialogHelper helper;
    private ArrayList<DataPekerjaan> listAllData = new ArrayList<>();

    private DatabaseReference refPekerjaanDatabase, refUserDatabase, refDesaUsers;

    private static final long UPDATE_INTERVAL = 0;
    private static final long FASTEST_UPDATE_INTERVAL = 1000 * 120 * 1;
    private static final long MAX_WAIT_TIME = UPDATE_INTERVAL * 3;

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    private static String LAT, LNG;

    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;

    private FirebaseAuth mAuth;
    private String mCurrentUserId;

    private RelativeLayout layoutEmpty;

    private ImageView inspekBackImg, inspekImg;
    private TextView inpekNameTv, inspekTvEmail;

    private String URL_STATISTIK = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        layoutEmpty = findViewById(R.id.layout_empty);


        FirebaseApp.initializeApp(this);

        mAuth = FirebaseAuth.getInstance();
        mCurrentUserId = mAuth.getCurrentUser().getUid();

        refPekerjaanDatabase = FirebaseDatabase.getInstance().getReference().child("pekerjaan");
        refUserDatabase = FirebaseDatabase.getInstance().getReference().child("users");
        refDesaUsers = FirebaseDatabase.getInstance().getReference().child("desa");

        layoutMenu = findViewById(R.id.my_layout_menu);
        tvName = findViewById(R.id.tv_navbar_name);
        tvJabatan = findViewById(R.id.tv_navbar_jabatan);
        imgUser = findViewById(R.id.img);
        layoutLogOut = findViewById(R.id.layout_logout);
        layoutTambah = findViewById(R.id.layout_tambah_pekerjaan);

        drawerLayout = findViewById(R.id.drawer_layout);

        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        helper = new AlertDialogHelper(HomeActivity.this);
        getIdDesa();
        init();
        helper.alertLoading();

        if (!checkPermissions()) {
            requestPermissions();
        }

        buildGoogleApiClient();

        layoutMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);

            }
        });

        layoutTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, TambahPekerjaanActivity.class));
            }
        });

        tvName.setText(mCurrentUserId);

        layoutLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session = new SessionManager(getApplicationContext());
                session.logoutUser();
                FirebaseAuth.getInstance().signOut();
                finishAffinity();
            }
        });

        findViewById(R.id.layout_info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogInformasiInspektorat();
            }
        });

        mRecyclerView = findViewById(R.id.my_home_rv);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        initUserData();


        findViewById(R.id.layout_search_pekerjaan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.layout_toolbar).setVisibility(View.GONE);
                findViewById(R.id.layout_search).setVisibility(View.VISIBLE);
            }
        });

        findViewById(R.id.layout_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.layout_toolbar).setVisibility(View.VISIBLE);
                findViewById(R.id.layout_search).setVisibility(View.GONE);
            }
        });

        EditText etSearch = findViewById(R.id.et_search);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    String a = s.toString().toLowerCase();
                    ArrayList<DataPekerjaan> newList = new ArrayList<>();
                    for (DataPekerjaan userInfo : listAllData) {
                        String username = userInfo.getKategori().toLowerCase();
                        if (username.contains(a)) {
                            newList.add(userInfo);
                        } else {
                            String tahun = String.valueOf(userInfo.getTahun());
                            if (tahun.contains(a)) {
                                newList.add(userInfo);
                            }
                        }
                    }
                    adapter.setFilter(newList);
                } catch (Exception e) {

                }

            }
        });

        findViewById(R.id.layout_kunjungan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, KunjunganActivity.class));
            }
        });
        findViewById(R.id.layout_statistik).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.addCategory(Intent.CATEGORY_BROWSABLE);
                    intent.setData(Uri.parse(URL_STATISTIK));
                    startActivity(intent);
                }catch (Exception e){

                }
            }
        });

    }

    private void getIdDesa() {
        session = new SessionManager(getApplicationContext());
        if (session.isLoggedIn()) {


            HashMap<String, String> user = session.getUserDetails();

            idDesa = user.get(SessionManager.DESA);
            Log.d(TAG, "ID Desa "+idDesa);
            if(idDesa.equals("D2")){
                URL_STATISTIK = "https://drive.google.com/file/d/16crk_4LR3MNEBl6m4LAkuIjo8I-XqRWV/view?usp=sharing";
            }else if(idDesa.equals("D1")) {
                URL_STATISTIK = "https://drive.google.com/file/d/1YCwdKNCOq_Z3uCfKk_5SXzKuUeQpoOaU/view?usp=sharing";
            }else {
                Toast.makeText(HomeActivity.this, "Maaf Grafik Belum Tersedia", Toast.LENGTH_SHORT).show();
            }

        }
    }

    void initUserData() {
        refUserDatabase.child(mCurrentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {
                    Log.d(TAG, "User " + dataSnapshot.child("nama").getValue());
                    tvName.setText("" + dataSnapshot.child("nama").getValue());
                    String id = String.valueOf(dataSnapshot.child("desa").getValue());

                    initUserDesa(id);

                    Glide.with(HomeActivity.this)
                            .load(dataSnapshot.child("gambar").getValue())
                            .apply(new RequestOptions()
                                    .placeholder(R.mipmap.jembatan)
                                    .error(R.mipmap.jembatan))
                            .into(imgUser);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    void initUserDesa(String id) {
        refDesaUsers.child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {
                    String namaDesa = dataSnapshot.child("nama").getValue().toString();
                    Log.d(TAG, "Desa " + namaDesa);

                    tvJabatan.setText("" + namaDesa);


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void init() {

        refPekerjaanDatabase.child(idDesa).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Log.d(TAG, "exists");

                    listAllData.clear();
                    helper.closeAlert();
//
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Log.d(TAG, "datasnapshot " + dataSnapshot.getKey());

                        String id = snapshot.getKey();

                        Log.d(TAG, "snapshot " + id);


                        DataPekerjaan d = snapshot.getValue(DataPekerjaan.class);

                        d.setmKey(id);

                        listAllData.add(d);

                    }
//
                    if (listAllData.size() == 0) {
                        layoutEmpty.setVisibility(View.VISIBLE);
                    } else layoutEmpty.setVisibility(View.GONE);
                    adapter = new HomeAdapter(mRecyclerView, HomeActivity.this, listAllData);
                    mRecyclerView.setAdapter(adapter);
//


                } else {
                    helper.closeAlert();
                    layoutEmpty.setVisibility(View.VISIBLE);
                    Log.d(TAG, "no exists");

                    listAllData.clear();
                    adapter = new HomeAdapter(mRecyclerView, HomeActivity.this, listAllData);
                    mRecyclerView.setAdapter(adapter);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    private void dialogInformasiInspektorat() {
        Dialog dialogDetail = new Dialog(this);

        dialogDetail.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogDetail.setContentView(R.layout.dialog_info);

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        Window window = dialogDetail.getWindow();
        layoutParams.copyFrom(window.getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialogDetail.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setAttributes(layoutParams);
        dialogDetail.setCancelable(true);

        inspekBackImg = dialogDetail.findViewById(R.id.inspek_back);
        inspekImg = dialogDetail.findViewById(R.id.inspek_img);
        inpekNameTv = dialogDetail.findViewById(R.id.inspek_name);
        inspekTvEmail = dialogDetail.findViewById(R.id.inspek_email);


//        Glide.with(this)
//                .load(d.getGambar())
//                .apply(new RequestOptions()
//                        .placeholder(R.mipmap.kamera)
//                        .error(R.mipmap.kamera))
//                .into(img);

        initInfoInspektorat();

        dialogDetail.show();
    }

    private void initInfoInspektorat() {

        refUserDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    String role = snapshot.child("role").getValue().toString();

                    if (role.equals("1")) {
                        String nama = snapshot.child("nama").getValue().toString();
                        String img = snapshot.child("gambar").getValue().toString();
                        String kontak = snapshot.child("kontak").getValue().toString();
                        String email = snapshot.child("email").getValue().toString();

                        inpekNameTv.setText(nama);
                        inspekTvEmail.setText(email + " | " + kontak);

                        Glide.with(HomeActivity.this)
                                .load(img)
                                .apply(new RequestOptions()
                                        .placeholder(R.mipmap.jembatan)
                                        .error(R.mipmap.jembatan))
                                .into(inspekBackImg);

                        Glide.with(HomeActivity.this)
                                .load(img)
                                .apply(new RequestOptions()
                                        .placeholder(R.mipmap.kamera)
                                        .error(R.mipmap.kamera))
                                .into(inspekImg);

                        Log.d(TAG, "nama Inspetorat " + nama);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        Log.d(TAG, "Location update started ..............: ");
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            Snackbar.make(
                    findViewById(R.id.TambahPekerjaanActivity),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(HomeActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    })
                    .show();
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(HomeActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        mLocationRequest.setInterval(UPDATE_INTERVAL);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Sets the maximum time when batched location updates are delivered. Updates may be
        // delivered sooner than this interval.
        mLocationRequest.setMaxWaitTime(MAX_WAIT_TIME);
    }

    private void buildGoogleApiClient() {
        if (mGoogleApiClient != null) {
            return;
        }
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "GoogleApiClient connected");
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

        LAT = String.valueOf(location.getLatitude());
        LNG = String.valueOf(location.getLongitude());

        Log.d(TAG, "onLocationChanged at Lat " + LAT);

        try {
            refUserDatabase.child(mCurrentUserId).child("pos").setValue(LAT + "," + LNG).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Log.d(TAG, "onLocationChanged : Success update location");
                }
            });
        } catch (Exception e) {

        }

    }
}
