package com.userundie.simpedes.aparatdesa.home;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.daimajia.numberprogressbar.NumberProgressBar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.userundie.simpedes.R;
import com.userundie.simpedes.activity.RegisStepOneActivity;
import com.userundie.simpedes.activity.RegisStepThreeActivity;
import com.userundie.simpedes.activity.RegisStepTwoActivity;
import com.userundie.simpedes.clas.AlertDialogHelper;
import com.userundie.simpedes.apihelper.MySingleton;
import com.userundie.simpedes.apihelper.UtilsApi;
import com.userundie.simpedes.handler.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class TambahPekerjaanActivity extends AppCompatActivity implements LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private RelativeLayout layoutBrowse;
    private String TAG = "TambahPekerjaanActivity";
    private ImageView img;

    private static int PICK_IMAGE_REQUEST = 1;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    final static int PICK_PDF_CODE = 2342;

    private Uri filePath;
    private Bitmap bitmap=null;

    public static String GAMBAR = "", NAMA= "", ID_USER="", USERNAME="", LAT="", LNG="", ID_DESA;
    private AlertDialogHelper helper;

    private EditText etNama;

    private static final long UPDATE_INTERVAL = 0;
    private static final long FASTEST_UPDATE_INTERVAL = 1000 * 120 * 1;
    private static final long MAX_WAIT_TIME = UPDATE_INTERVAL * 3;


    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;

    private Uri resultUri;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase, pekerjaanDatabase, progressDatabase;
    private StorageReference mImageStorage, mFileStorage;
    private StorageTask mUploadTask;

    private static final String UPLOAD_PATH_STORAGE = "pekerjaan";

    public static final String FILE_PATH_UPLOADS = "uploads_ard";

    private Button mButtonAdd;
    private NumberProgressBar progressFile;

    private String fileNamePath;


    private String resKategori = "";
    private RadioButton rbDrenase, rbJembatan, rbPaving, rbJalan, rbTalud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_pekerjaan);

        layoutBrowse = findViewById(R.id.layout_browse);
        img = findViewById(R.id.img_add);
        etNama = findViewById(R.id.etNama);
        mButtonAdd = findViewById(R.id.my_item_detail_pengerjaan_btn_lihats);
        progressFile = findViewById(R.id.progress_file);

        mButtonAdd.setVisibility(View.GONE);
        progressFile.setVisibility(View.GONE);


        mAuth = FirebaseAuth.getInstance();
        mImageStorage = FirebaseStorage.getInstance().getReference().child(UPLOAD_PATH_STORAGE);
        mFileStorage = FirebaseStorage.getInstance().getReference().child(FILE_PATH_UPLOADS);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        helper = new AlertDialogHelper(TambahPekerjaanActivity.this);

        layoutBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    showFileChooser();

            }
        });



        if (!checkPermissions()) {
            requestPermissions();
        }

        buildGoogleApiClient();


        SessionManager session = new SessionManager(getApplicationContext());
        if (session.isLoggedIn()) {


            HashMap<String, String> user = session.getUserDetails();


            ID_DESA = user.get("desa");

            Log.i(TAG, "Desa "+ID_DESA);



        }

        rbDrenase = findViewById(R.id.rb_kat_dre);
        rbJalan = findViewById(R.id.rb_kat_jal);
        rbJembatan = findViewById(R.id.rb_kat_jem);
        rbPaving = findViewById(R.id.rb_kat_pav);
        rbTalud = findViewById(R.id.rb_kat_tal);

        findViewById(R.id.rb_kat_dre).setOnClickListener(this);
        findViewById(R.id.rb_kat_jem).setOnClickListener(this);
        findViewById(R.id.rb_kat_pav).setOnClickListener(this);
        findViewById(R.id.rb_kat_jal).setOnClickListener(this);
        findViewById(R.id.rb_kat_tal).setOnClickListener(this);


    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            Snackbar.make(
                    findViewById(R.id.TambahPekerjaanActivity),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(TambahPekerjaanActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    })
                    .show();
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(TambahPekerjaanActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    private void buildGoogleApiClient() {
        if (mGoogleApiClient != null) {
            return;
        }
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        mLocationRequest.setInterval(UPDATE_INTERVAL);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Sets the maximum time when batched location updates are delivered. Updates may be
        // delivered sooner than this interval.
        mLocationRequest.setMaxWaitTime(MAX_WAIT_TIME);
    }

    private void showFileChooser() {
        CropImage.activity(filePath)
                .setAspectRatio(200,100)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }
    private String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        try {
            filePath = data.getData();




            if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK){
                filePath = data.getData();


//            try {
//                bitmap = MediaStore.Images.Media.getBitmap(RegisStepOneActivity.this.getContentResolver(), filePath);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }

                CropImage.activity(filePath)
                        .setAspectRatio(200,100)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);

            }
            if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);

                if (resultCode == RESULT_OK) {

                    Uri resultUri = result.getUri();
                    this.resultUri = resultUri;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(TambahPekerjaanActivity.this.getContentResolver(), resultUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

//                mProgressBar.setVisibility(View.VISIBLE);
//                uploadFile(result, bitmap);

                    File f = new File(resultUri.getPath());
                    long size = f.length() / 1024;


                    Log.d(TAG, " path " + resultUri.getPath() + " Size " + f.length() / 1024);

                    if (size < 1024) {

                        img.setImageBitmap(bitmap);
                        GAMBAR = getStringImage(bitmap);
//                        Log.d(TAG, "result gambar "+GAMBAR);

                    } else
                        Toast.makeText(TambahPekerjaanActivity.this, "Maaf size maximum 1024 Kb", Toast.LENGTH_SHORT).show();

//                Log.d(TAG, "gambar "+GAMBAR);

                }
            }

            if (requestCode == PICK_PDF_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
                //if a file is selected
                if (data.getData() != null) {
                    //uploading the file

                    File file = new File(data.getData().getPath());

                    ContentResolver contentResolver = getContentResolver();
                    MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();

                    // Return file Extension
                    String ext = mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(data.getData()));
                    Log.d("UbahMateriActivity", "File lagi " + ext);
                    Log.d("UbahMateriActivity", "File : " + file.getName());

                    uploadFile(data.getData(), ext);

                } else {
                    Toast.makeText(this, "No file chosen", Toast.LENGTH_SHORT).show();
                }
            }
        }catch (Exception e){

        }

    }

    public void tambahPekerjaanClick(View view) {
        NAMA = etNama.getText().toString();

        if(!GAMBAR.equals("")&& !NAMA.equals("")&& !ID_DESA.equals("")&& !LAT.equals("")&& !LNG.equals("")&& !fileNamePath.equals("")){
            insertData();
            helper.alertLoading();
        }else helper.alertError("Silahkan isi semua kolom");
    }
    private void insertData() {


        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        final String uid = currentUser.getUid();

        final StorageReference mStorageReference = mImageStorage.child(ID_DESA+NAMA+".jpg");
        mUploadTask = mStorageReference.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                if(task.isSuccessful()){
                    final Task<Uri> urlTask = mUploadTask.continueWithTask(new Continuation() {
                        @Override
                        public Object then(@NonNull Task task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }

                            // Continue with the task to get the download URL
                            return mStorageReference.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {

                            if(task.isSuccessful()){
                                Uri uri = task.getResult();

                                final String uploaded_image_url = uri.toString();

                                final HashMap<String, String> usMap = new HashMap<>();

                                pekerjaanDatabase = mDatabase.child("pekerjaan").child(ID_DESA).child(ID_DESA+NAMA);

                                usMap.put("nama", NAMA);
                                usMap.put("user", uid);
                                usMap.put("pos", LAT+","+LNG);
                                usMap.put("gambar", uploaded_image_url);
                                usMap.put("file", fileNamePath);
                                usMap.put("kategori", resKategori);
                                usMap.put("tahun", String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));




                                pekerjaanDatabase.setValue(usMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                        progressDatabase = mDatabase.child("progress").child(ID_DESA+NAMA).child(ID_DESA+NAMA+"0");

                                        HashMap<String, String> proMap = new HashMap<>();
                                        proMap.put("persen", "0");
                                        proMap.put("detail", "-");
                                        proMap.put("catatan", "-");
                                        proMap.put("gambar", uploaded_image_url);

                                        progressDatabase.setValue(proMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                helper.alertSuccesCloseDialog("Data Berhasil ditambahkan");


                                            }
                                        });


                                    }
                                });
                            }

                        }
                    });

                }else {

                    helper.closeAlert();
                    Toast.makeText(TambahPekerjaanActivity.this, "Terjadi kesalahan, silahkan coba lagi", Toast.LENGTH_LONG).show();

                }

            }
        });



    }

    private void uploadFile(final Uri data, String ext) {

        progressFile.setVisibility(View.VISIBLE);
        progressFile.setProgress(0);

        final StorageReference sRef = mFileStorage.child(FILE_PATH_UPLOADS + System.currentTimeMillis() + "." + ext);

        mUploadTask = sRef.putFile(data).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                if (task.isSuccessful()) {
                    final Task<Uri> urlTask = mUploadTask.continueWithTask(new Continuation() {
                        @Override
                        public Object then(@NonNull Task task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }

                            // Continue with the task to get the download URL
                            return sRef.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {

                            if (task.isSuccessful()) {
                                Uri uri = task.getResult();

                                String uploaded_file_url = uri.toString();

                                fileNamePath = uploaded_file_url;
                                Log.d(TAG, "File name path "+uploaded_file_url);
                                mButtonAdd.setVisibility(View.VISIBLE);

                            }

                        }
                    });


                } else {

                    Toast.makeText(TambahPekerjaanActivity.this, "Error in uploading", Toast.LENGTH_LONG).show();

                }

            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
//
                progressFile.setProgress((int) progress);

            }
        });


    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "GoogleApiClient connected");
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i(TAG, "onLocationChanged");

        LAT = String.valueOf(location.getLatitude());
        LNG = String.valueOf(location.getLongitude());
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        Log.d(TAG, "Location update started ..............: ");
    }

    public void browseClick(View view) {
        Intent intent = new Intent();
        intent.setType("application/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select AD/ART File"), PICK_PDF_CODE);
    }

    public void layoutBackClick(View view) {
        onBackPressed();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.rb_kat_dre : resKategori = rbDrenase.getText().toString(); break;
        }
    }
}
