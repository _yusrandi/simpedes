package com.userundie.simpedes.aparatdesa.home.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.userundie.simpedes.R;
import com.userundie.simpedes.aparatdesa.home.TambahPekerjaanActivity;
import com.userundie.simpedes.apihelper.UtilsApi;
import com.userundie.simpedes.handler.SessionManager;
import com.userundie.simpedes.model.DataProgress;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import static android.app.Activity.RESULT_OK;

public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.MyViewHolder> {


    private RecyclerView mRecyclerView;
    private Context mContext;
    private ArrayList<DataProgress> list;
    private String TAG = "DetailAdapter";

    private static int PICK_IMAGE_REQUEST = 1;


    private ImageView img;


    private OnClickListener mListener;

    private SessionManager session;
    private String userRule="";

    public DetailAdapter(RecyclerView mRecyclerView, Context mContext, ArrayList<DataProgress> list) {
        this.mRecyclerView = mRecyclerView;
        this.mContext = mContext;
        this.list = list;

        session = new SessionManager(mContext);
        if (session.isLoggedIn()) {


            HashMap<String, String> user = session.getUserDetails();


            userRule = user.get(SessionManager.RULE) ;

        }
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_detail_pekerjaan, viewGroup, false);



        MyViewHolder ViewHolder = new MyViewHolder(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = mRecyclerView.getChildLayoutPosition(view);
//
//                dataService data = list.get(position);
//                getDetailDialog(data);

//                viewDetail(data);

//                mContext.startActivity(new Intent(mContext, DetailPekerjaanActivity.class));


            }
        });

        return new DetailAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {

        try {
            myViewHolder.image.setVisibility(View.VISIBLE);
            Glide.with(mContext)
                    .load(list.get(i).getGambar())
                    .apply(new RequestOptions()
                            .placeholder(R.mipmap.jembatan)
                            .error(R.mipmap.jembatan))
                    .into(myViewHolder.image);

            Log.i(TAG, list.get(i).toString());
            myViewHolder.tvPengerjaan.setText(list.get(i).getPersen() + "% Pengerjaan");



        } catch (Exception e) {


            myViewHolder.image.setVisibility(View.GONE);
            Log.i(TAG, e.toString());
            myViewHolder.tvPengerjaan.setText("Belum Pengerjaan");



        }


        for (int x = 0; x < 5; x++) {
//            myViewHolder.tvPengerjaan.setText(25 * i + "% Pengerjaan");
        }


        myViewHolder.btnLihat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if(mListener!=null){
                        mListener.onLihatClick(i);
                    }

                } catch (Exception e) {
                    Toast.makeText(mContext, "Anda belum menambahkan ", Toast.LENGTH_SHORT).show();

                }

            }
        });
        myViewHolder.btnPilih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataProgress progress = null;

                try {

                    if(mListener!=null){
                        mListener.onPilihClick(i, "sudah");
                    }

                } catch (Exception e) {

                    if(mListener!=null){
                        mListener.onPilihClick(i, "belum");
                    }

                }


            }
        });
    }

    @Override
    public int getItemCount() {
        if(userRule.equals("1")){
            return list.size();
        }else return 3;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView tvPengerjaan;
        public Button btnPilih, btnLihat;
        public ImageView image;
        public CardView parentLayout;


        public MyViewHolder(View view) {
            super(view);

            tvPengerjaan = (TextView) view.findViewById(R.id.my_item_detail_pengerjaan);
            btnPilih = view.findViewById(R.id.my_item_detail_pengerjaan_btn_pilih);
            btnLihat = view.findViewById(R.id.my_item_detail_pengerjaan_btn_lihats);
            image = view.findViewById(R.id.image);
            parentLayout = view.findViewById(R.id.layout_parent);


        }


    }



    public interface OnClickListener {
        void onPilihClick(int position, String status);
        void onLihatClick(int position);

    }
    public void setOnClickListener(OnClickListener listener) {
        mListener = listener;
    }

}
